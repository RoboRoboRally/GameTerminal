# Game Terminal

Desktop application for the RoboRoboRally project.

# RoboRoboRally

As a part of masters study course at Faculty of Mathematics and Physics, Charles University, we were required to complete a team project. Students should experience and complete every aspect of software development during this project, which includes: defining the project, selecting team members, designing the system and choosing technologies, writing specification, presenting specification to the stakeholders, project organization and planning, implementation, testing, writing documentation and finishing the project (deployment/delivery).

Our team consisted of 4 members. The goal of our project was to robotize a board game called RoboRally. This system would be then used as a propagation material for Charles University. To complete this goal, the project consists of 5 components.
1) Heavily modified 2D plotter, which moves figures on the board with magnets. Also includes custom controller and driver.
2) Mobile application which acts as a controller for a players to play cards.
3) Game terminal, which shows what is happening in the game to bystanders and players.
4) Model of the game, which handles rules and events of the game (essentialy digitizes the board game).
5) Server and communication library which connects 4 previous components together.

My job in this project was to create the mobile application and game terminal.
Václav Čamra handled the server and communication library. Also helped out with plotter and game terminal.
Andrej Čižmárik created the plotter robot.
Petr Šťavík made the model of the game.

The project acts nicely as a presentation material for the MFF UK, as we can show to potential students large part of what the informatics section teaches - software engineering, robotics, mobile development, desktop development and networking.