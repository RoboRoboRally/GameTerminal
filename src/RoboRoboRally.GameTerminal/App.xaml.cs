﻿using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Core;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;

namespace RoboRoboRally.GameTerminal
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        IConnectionServer server;
        Config.Config config = new Config.Config();

        protected override async void OnStartup(StartupEventArgs e)
        {
            server = new ConnectionServer(config.Port, config.UseHttps, config);
            server.Start();

            var viewModel = new GameWindowViewModel(server);
            await viewModel.Connect();

            var window = new GameWindowView
            {
                DataContext = viewModel
            };

            PresentationTraceSources.Refresh();
            PresentationTraceSources.DataBindingSource.Listeners.Add(new ConsoleTraceListener());
            PresentationTraceSources.DataBindingSource.Listeners.Add(new DebugTraceListener());
            PresentationTraceSources.DataBindingSource.Switch.Level = SourceLevels.Warning | SourceLevels.Error;
            window.Show();
        }

        private void OnExit(object sender, ExitEventArgs e)
        {
			Environment.Exit(0);
        }
    }

    [CompilerGenerated]
    public class DebugTraceListener : TraceListener
    {
        public override void Write(string message)
        {
        }

        public override void WriteLine(string message)
        {
            Debugger.Break();
        }
    }
}
