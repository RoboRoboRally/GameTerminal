﻿using RoboRoboRally.GameTerminal.Communication;
using RoboRoboRally.GameTerminal.CustomControls;
using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.GameTerminal.Properties;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Core;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Input;

namespace RoboRoboRally.GameTerminal.ViewModels
{
    /// <summary>
    /// ViewModel for the GameWindowView
    /// Handles calls from server, buttons and buttons to manipulate the game (start, stop ...)
    /// Holds ViewModels for the other Views used to display the game screen
    /// </summary>
    public class GameWindowViewModel : BaseModel
    {
        /// <summary>
        /// One PlayerViewModel for each robot in the game.
        /// Used as a datacontext for the PlayerView
        /// </summary>
        public ObservableCollection<PlayerViewModel> Players { get; set; } = new ObservableCollection<PlayerViewModel>();

        /// <summary>
        /// Used to disable/enable the start button
        /// </summary>
        public bool AtleastOnePickedPlayer => Players.Where(p => p.IsPicked).Any();

        private MapsViewModel map;
        /// <summary>
        /// ViewModel for both maps in the center
        /// </summary>
        public MapsViewModel Map
        {
            get => map;
            set => SetValue(ref map, value);
        }

        private GameSetupViewModel gameSetup;
        /// <summary>
        /// ViewModel for the game setup
        /// </summary>
        public GameSetupViewModel GameSetup
        {
            get => gameSetup;
            set => SetValue(ref gameSetup, value);
        }

        /// <summary>
        /// Functions assigned during map setup, used in TileViewModel
        /// </summary>
        public Action<TileModel> TileModificationFunction = delegate { };
        public Action RefreshView = delegate { };

        /// <summary>
        /// Collection of messages displayed by the logger
        /// </summary>
        public ObservableCollection<object> LogMessages { get; private set; } = new ObservableCollection<object>();

        /// <summary>
        /// Settings of the game
        /// </summary>
        public GameSettings Settings { get; set; } = new GameSettings
        {
            MapSettings = new MapSettings(),
            IncludeUpgrades = new Config.Config().IncludeUpgrades
        };

        /// <summary>
        /// The option to use or not to use the upgrades
        /// </summary>
        public bool IncludeUpgrades
        {
            get => Settings.IncludeUpgrades;
            set
            {
                Settings.IncludeUpgrades = value;
                new Config.Config().IncludeUpgrades = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Pack of cards used during the game
        /// </summary>
        public BindingList<ProgrammingCardCount> ProgrammingPack { get; set; } = new BindingList<ProgrammingCardCount>();

        private bool inSetup = false;
        /// <summary>
        /// To indicate of the player is setting up the game
        /// </summary>
        public bool InSetup
        {
            get => inSetup;
            set => SetValue(ref inSetup, value);
        }

        private bool isGameRunning = false;
        /// <summary>
        /// To indicate that the game is running - used to disable / enable buttons
        /// </summary>
        public bool IsGameRunning
        {
            get => isGameRunning;
            set
            {
                SetValue(ref isGameRunning, value);
                OnPropertyChanged(nameof(CanIssuePause));
            }
        }

        private bool isGamePaused = false;
        /// <summary>
        /// To indicate that the game is paused - used to disable / enable buttons
        /// </summary>
        public bool IsGamePaused
        {
            get => isGamePaused;
            set
            {
                SetValue(ref isGamePaused, value);
                IsPauseIssued = false;
            }
        }

        private bool isPauseIssued = false;
        /// <summary>
        /// To indicate that we are waiting for a pause - used to disable / enable buttons
        /// </summary>
        public bool IsPauseIssued
        {
            get => isPauseIssued;
            set
            {
                SetValue(ref isPauseIssued, value);
                OnPropertyChanged(nameof(CanIssuePause));
            }
        }

        /// <summary>
        /// To indicate that we can pause the game - used to disable / enable buttons
        /// </summary>
        public bool CanIssuePause => !isPauseIssued && IsGameRunning;

        /// <summary>
        /// To indicate that the game can be started - used to disable / enable buttons
        /// </summary>
        public bool CanStartGame => !IsGameRunning
            && !InSetup
            && AtleastOnePickedPlayer
            && Map.SettingsValid
            && ProgrammingPack.Aggregate(0, (sum, card) => sum + card.CardCount) >= config.MinProgrammingCardCount;

        /// <summary>
        /// Lobby of the game, where players can join
        /// </summary>
        public IOwnedLobby Lobby { get; set; }

        /// <summary>
        /// Instance of the game - used to stop, pause and start the game
        /// </summary>
        public IGame Game => connection.Game;

        private IConnectionServer connectionServer;
        private TerminalConnection connection;

        private Config.Config config = new Config.Config();

        private bool pluginBeingManipulated = false;
        public bool PluginBeingManipulated
        {
            get => pluginBeingManipulated;
            set => SetValue(ref pluginBeingManipulated, value);
        }

        System.Windows.Threading.Dispatcher dispatcher = Application.Current.Dispatcher;

        /// <summary>
        /// In this constructor we enable the logger, create MapsViewModel, GameSetupViewModel and setup the pack of cards
        /// </summary>
        /// <param name="connectionServer"></param>
        public GameWindowViewModel(IConnectionServer connectionServer)
        {
            StartLogging(connectionServer);

            PropertyChanged += (__, args) =>
            {
                if (args.PropertyName == nameof(IsGameRunning)
                || args.PropertyName == nameof(InSetup)
                || args.PropertyName == nameof(AtleastOnePickedPlayer))
                    OnPropertyChanged(nameof(CanStartGame));
            };

            SetupProgrammingPack();
            Map = new MapsViewModel(this);
            Map.PropertyChanged += (__, args) =>
            {
                if (args.PropertyName == nameof(Map.SettingsValid))
                    OnPropertyChanged(nameof(CanStartGame));
            };

            GameSetup = new GameSetupViewModel(this);
        }

        private void StartLogging(IConnectionServer connectionServer)
        {
            this.connectionServer = connectionServer;
            LogMessages.Add(Resources.ServerStarted);
            connection = new TerminalConnection(m =>
            {
                dispatcher.Invoke(() =>
                {
                    LogMessages.Add(m);
                });
                Task.Delay(250).Wait();
            }, this);
        }

        private void SetupProgrammingPack()
        {
            var cardLoader = new CardLoader($"{config.AbsoluteGameDataFolder}/DB/CardDB");
            foreach (var card in cardLoader.LoadPlayerProgrammingCards())
            {
                ProgrammingPack.Add(new ProgrammingCardCount { CardName = card.Name, CardCount = 1 });
            }

            ProgrammingPack.ListChanged += (_, __) =>
            {
                OnPropertyChanged(nameof(CanStartGame));
            };

            var lastSettings = config.ProgrammingPackLastUsed;
            if (lastSettings == null || lastSettings.Count == 0)
                lastSettings = config.ProgrammingPackDefaults;

            foreach (var (card, count) in lastSettings)
            {
                var pack = ProgrammingPack.Where(p => p.CardName == card).FirstOrDefault();
                if (pack == null)
                    continue;

                pack.CardCount = count;
            }
        }

        /// <summary>
        /// Establish connection to the server and add players for each robot
        /// </summary>
        /// <returns></returns>
        public async Task Connect()
        {
            string address = GetIPAdress();
            IServer serverAPI = connectionServer.AddLocalConnection(connection);
            await Task.Run(() => Lobby = serverAPI.CreateLobby());

            LogMessages.Add(string.Format(Resources.LobbyCreated, Lobby.GetLobbyId()));

            foreach (var robot in Lobby.GetAllRobots())
            {
                Players.Add(new PlayerViewModel(this, robot, new PlayerQrConnectionData
                {
                    Ip = address,
                    LobbyName = Lobby.GetLobbyId(),
                    Port = config.Port,
                    RobotName = robot.Name
                }));
                //LogMessages.Add(string.Format(Resources.LobbyContainsRobot, robot.Name, robot.Color));
            }
        }

        public void ResetPackToDefault()
        {
            var defaults = config.ProgrammingPackDefaults;

            foreach (var pack in ProgrammingPack)
            {
                var def = ProgrammingPack.Where(p => p.CardName == pack.CardName).FirstOrDefault();
                pack.CardCount = def?.CardCount ?? 0;
            }

            foreach (var (card, count) in defaults)
            {
                var pack = ProgrammingPack.Where(p => p.CardName == card).FirstOrDefault();
                if (pack == null)
                    continue;

                pack.CardCount = count;
            }
        }

        private string GetIPAdress()
        {
            var addresses = Dns.GetHostEntry((Dns.GetHostName()))
                                .AddressList
                                .Where(x => x.AddressFamily == AddressFamily.InterNetwork)
                                .Select(x => x.ToString())
                                .ToArray();

            foreach (var adress in addresses)
            {
                LogMessages.Add(string.Format(Resources.IPAdressDetected, adress));
            }

            if (addresses.Length == 0)
            {
                LogMessages.Add(string.Format(Resources.NoIPAdress));
                return "";
            }
            else
            {
                if (addresses.Length > 1)
                {
                    LogMessages.Add(string.Format(Resources.TooManyIPAdresses));
                    while (true)
                    {
                        for (int i = 0; i < addresses.Length; i++)
                        {
                            MessageBoxResult messageBoxResult = MessageBox.Show(string.Format(Resources.UseIpQuestion, addresses[i]), string.Format(Resources.MultipleIpFound, i + 1, addresses.Length), MessageBoxButton.YesNo);
                            if (messageBoxResult == MessageBoxResult.Yes)
                            {
                                LogMessages.Add(string.Format(Resources.UsingIp, addresses[i]));
                                return addresses[i];
                            }
                        }
                    }
                }
                else
                {
                    LogMessages.Add(string.Format(Resources.UsingIp, addresses[0]));
                    return addresses[0];
                }
            }
        }

        /// <summary>
        /// Clean programming cards of all players
        /// </summary>
        public void CleanProgrammingCards()
        {
            foreach (var player in Players)
            {
                foreach (var card in player.ProgrammingCards)
                {
                    card.Card.CardId = Constants.programmingCardBack;
                }
            }
        }


        // ------------ Communication ------------ 

        /// <summary>
        /// Finds the player with given robot, replaces card in the register with new one
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="register"></param>
        /// <param name="card"></param>
        public void CardRevealed(RobotInfo robot, int register, string card)
        {
            try
            {
                PlayerViewModel player = Players.Single(i => i.Robot == robot);
                player.ProgrammingCards[register - 1].Card.CardId = card;
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Player scaned a robots QR code and connected to the game
        /// Mark the robot as picked, which as a result will show the players cards
        /// </summary>
        /// <param name="robot"></param>
        public void RobotPicked(RobotInfo robot)
        {
            try
            {
                PlayerViewModel player = Players.Single(i => i.Robot == robot);
                player.IsPicked = true;
                OnPropertyChanged(nameof(AtleastOnePickedPlayer));
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Player with a given robot disconnected from a game
        /// Mark the robot as free, which results in showing the QR code again
        /// </summary>
        /// <param name="robot"></param>
        public void RobotFreed(RobotInfo robot)
        {
            try
            {
                PlayerViewModel player = Players.Single(i => i.Robot == robot);
                player.IsPicked = false;
                OnPropertyChanged(nameof(AtleastOnePickedPlayer));                
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Check whether the position is on the main or starting map
        /// If the position is out of the map, remove the robot
        /// </summary>
        /// <param name="player"></param>
        /// <param name="coordinates"></param>
        private void AssignPlayerPosition(PlayerViewModel player, MapCoordinates coordinates)
        {
            int x = coordinates.RowIndex;
            int y = coordinates.ColIndex;
            int maxX = Map.SelectedMainMap.MapSize.RowCount + Map.SelectedStartingMap.MapSize.RowCount - 1;
            int maxY = Map.SelectedMainMap.MapSize.ColCount - 1;

            if (x < 0 || x > maxX || y < 0 || y > maxY)
            {
                RemovePlayerPosition(player);
                return;
            }

            if (x > Map.SelectedMainMap.MapSize.RowCount - 1)
            {
                x = x - Map.SelectedMainMap.MapSize.RowCount;
                Map.StartingMapGrid.Rows[x].Tiles[y].PresentPlayer = player;
            }
            else
            {
                Map.MainMapGrid.Rows[x].Tiles[y].PresentPlayer = player;
            }
        }

        /// <summary>
        /// Checks the position, removes player from the map
        /// </summary>
        /// <param name="player"></param>
        public void RemovePlayerPosition(PlayerViewModel player)
        {
            int x = player.coordinates.RowIndex;
            int y = player.coordinates.ColIndex;
            int maxX = Map.SelectedMainMap.MapSize.RowCount + Map.SelectedStartingMap.MapSize.RowCount - 1;
            int maxY = Map.SelectedMainMap.MapSize.ColCount - 1;

            if (x < 0 || x > maxX || y < 0 || y > maxY)
            {
                return;
            }

            if (x > Map.SelectedMainMap.MapSize.RowCount - 1)
            {
                x = x - Map.SelectedMainMap.MapSize.RowCount;
                Map.StartingMapGrid.Rows[x].Tiles[y].PresentPlayer = null;
            }
            else
            {
                Map.MainMapGrid.Rows[x].Tiles[y].PresentPlayer = null;
            }
        }

        /// <summary>
        /// Removes player with a given robot from current position, assignes him to a new position and direction
        /// Also used for rotation with the same position
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="coordinates">new position</param>
        /// <param name="direction">new direction</param>
        public void RelocateRobot(RobotInfo robot, MapCoordinates coordinates, Direction direction)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            RemovePlayerPosition(player);
            player.coordinates = coordinates;
            player.direction = direction.GetDirectionType();
            AssignPlayerPosition(player, coordinates);
            Map.RefreshRays();
        }

        /// <summary>
        /// Removes player with a given robot from current position, assignes him to a new position and direction
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="coordinates"></param>
        public void RelocateRobot(RobotInfo robot, MapCoordinates coordinates)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            RemovePlayerPosition(player);
            player.coordinates = coordinates;
            AssignPlayerPosition(player, coordinates);
            Map.RefreshRays();
        }

        /// <summary>
        /// Removes player with given robot from the board
        /// </summary>
        /// <param name="robot"></param>
        public void RemoveRobot(RobotInfo robot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            RemovePlayerPosition(player);
        }

        /// <summary>
        /// Rotates player with a given robot
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="rotation"></param>
        public void RotateRobot(RobotInfo robot, RotationType rotation)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            DirectionType direction = player.direction;
            int dirInt = (int)direction;

            switch (rotation)
            {
                case RotationType.Left:
                    dirInt += 1;
                    if (dirInt > 3)
                    {
                        dirInt -= 4;
                    }

                    player.direction = (DirectionType)(dirInt);
                    RelocateRobot(robot, player.coordinates);
                    break;
                case RotationType.Right:
                    dirInt -= 1;
                    if (dirInt < 0)
                    {
                        dirInt += 4;
                    }

                    player.direction = (DirectionType)(dirInt);
                    RelocateRobot(robot, player.coordinates);
                    break;
                case RotationType.None:

                    break;
                case RotationType.Back:
                    dirInt += 2;
                    if (dirInt > 3)
                    {
                        dirInt -= 4;
                    }

                    player.direction = (DirectionType)(dirInt);
                    RelocateRobot(robot, player.coordinates);
                    break;

            }
        }

        /// <summary>
        /// Updates batteries of a player with given robot
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="newEnergy"></param>
        public void BatteriesChanged(RobotInfo robot, int newEnergy)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            player.BatteryCount = newEnergy;

        }

        /// <summary>
        /// Adds new upgrade to the permanent or temporary upgrade list of a player        
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="boughtCard"></param>
        public void CardBought(RobotInfo robot, PurchaseResponse boughtCard)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            if (boughtCard.SelectedCard.CardType == UpgradeCardType.Permanent)
            {
                ReplaceUpgrade(player.PermanentUpgrades, boughtCard);
            }
            else
            {
                ReplaceUpgrade(player.TemporaryUpgrades, boughtCard);
            }
        }

        /// <summary>
        /// If the player discarded a card in order to buy this upgrade, replace the discarded upgrade
        /// Otherwise just replace a card which shows the back of the card
        /// </summary>
        /// <param name="upgrades"></param>
        /// <param name="boughtCard"></param>
        public void ReplaceUpgrade(ObservableCollection<SlotModel> upgrades, PurchaseResponse boughtCard)
        {
            if (boughtCard.DiscardedCard == null)
            {
                SlotModel card = upgrades.First(i => i.Card.CardId == Constants.upgradeCardBack);
                card.Card.CardId = boughtCard.SelectedCard.CardId;
            }
            else
            {
                // Replace the discarded card
                SlotModel card = upgrades.Single(i => i.Card.CardId == boughtCard.DiscardedCard.CardId);
                card.Card.CardId = boughtCard.SelectedCard.CardId;
            }
        }

        /// <summary>
        /// Update the checkpoint count, if the player obtained next checkpoint in order
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="checkpointNr"></param>
        public void RobotVisitedCheckpoint(RobotInfo robot, int checkpointNr)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            if (player.LastPassedCheckpoint + 1 == checkpointNr)
            {
                player.LastPassedCheckpoint++;
            }
        }

        /// <summary>
        /// Player with a given robot is shooting his laser
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="shoot"></param>
        public void RobotLasers(RobotInfo robot, bool shoot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);

            if (shoot)
            {
                Map.RefreshRays(player);
            }
            else
            {
                Map.RefreshRays();
            }
        }

        /// <summary>
        /// Player with a given robot received damage, signal this by red overlay
        /// </summary>
        /// <param name="robot"></param>
        public void DamageSignal(RobotInfo robot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            player.DamageTaken = true;
            RelocateRobot(robot, player.coordinates);
        }

        /// <summary>
        /// Remove the red overlay
        /// </summary>
        /// <param name="robot"></param>
        public void DamageUnsignal(RobotInfo robot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            player.DamageTaken = false;
            RelocateRobot(robot, player.coordinates);
        }

        /// <summary>
        /// Player with a given robot used upgrade, signal this by yellow overlay
        /// </summary>
        /// <param name="robot"></param>
        public void UpgradeSignal(RobotInfo robot, CardInfo cardSelection)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            player.UpgradeUsed = true;
            try
            {
                SlotModel target = player.TemporaryUpgrades.Single(i => i.Card.CardId == cardSelection.CardId);
                target.Card.CardId = Constants.upgradeCardBack;
            }
            catch (Exception)
            {

            }
            RelocateRobot(robot, player.coordinates);
        }

        /// <summary>
        /// Remove the yellow overlay
        /// </summary>
        /// <param name="robot"></param>
        public void UpgradeUnsignal(RobotInfo robot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            player.UpgradeUsed = false;
            RelocateRobot(robot, player.coordinates);
        }

        /// <summary>
        /// Player with a given robot is rebooting
        /// </summary>
        /// <param name="robot"></param>
        public void Reboot(RobotInfo robot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            foreach (SlotModel card in player.ProgrammingCards)
            {
                if (card.Card.CardId == Constants.programmingCardBack)
                {
                    card.Card.CardId = Constants.invalidTexture;
                }
            }
        }

        public void HighlightCards(RobotInfo robot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            player.BackgroundColor = player.Robot.Color;
        }

        public void UnHighlightCards(RobotInfo robot)
        {
            PlayerViewModel player = Players.Single(i => i.Robot == robot);
            player.BackgroundColor = Constants.backgroundColor;
        }

        /// <summary>
        /// Clean the player as a preparation for new game
        /// </summary>
        public void Reset()
        {
            foreach (PlayerViewModel player in Players)
            {
                player.Clean();
            }
        }


        // ------------ GameWindow buttons ------------ 

        private ICommand startGameCommand;
        public ICommand StartGameCommand
        {
            get
            {
                return startGameCommand ?? (startGameCommand = new CommandHandler(() => StartGame(), true));
            }
        }

        private ICommand stopGameCommand;
        public ICommand StopGameCommand
        {
            get
            {
                return stopGameCommand ?? (stopGameCommand = new CommandHandler(() => StopGame(), true));
            }
        }

        private ICommand pauseGameCommand;
        public ICommand PauseGameCommand
        {
            get
            {
                return pauseGameCommand ?? (pauseGameCommand = new CommandHandler(() => PauseGame(), true));
            }
        }

        private ICommand resumeGameCommand;
        public ICommand ResumeGameCommand
        {
            get
            {
                return resumeGameCommand ?? (resumeGameCommand = new CommandHandler(() => ResumeGame(), true));
            }
        }

        private ICommand quitGameCommand;
        public ICommand QuitGameCommand
        {
            get
            {
                return quitGameCommand ?? (quitGameCommand = new CommandHandler(() => QuitGame(), true));
            }
        }

        private ICommand setupMapCommand;
        public ICommand SetupMapCommand
        {
            get
            {
                return setupMapCommand ?? (setupMapCommand = new CommandHandler(() => SetupMap(), true));
            }
        }

        private ICommand openManagePluginsWindowCommand;
        public ICommand OpenManagePluginsWindowCommand
        {
            get
            {
                return openManagePluginsWindowCommand ?? (openManagePluginsWindowCommand = new CommandHandler(() => OpenManagePluginsWindow(), true));
            }
        }

        private async void StartGame()
        {
            LogMessages.Clear();// ClearLog();
            await Task.Run(() => StartGameAsync(this));
        }

        private void StartGameAsync(GameWindowViewModel viewModel)
        {
            GameSettings settings = viewModel.Settings;
            settings.MapSettings = viewModel.Map.CreateSettings();
            settings.ProgrammingPack = new Dictionary<CardInfo, int>();
            foreach (var countData in viewModel.ProgrammingPack)
            {
                settings.ProgrammingPack.Add(new CardInfo { CardId = countData.CardName }, countData.CardCount);
            }

            try
            {
                foreach (var player in viewModel.Players)
                {
                    player.BatteryCount = 5;
                    player.LastPassedCheckpoint = 0;
                    player.Won = false;
                }
                viewModel.Lobby.StartGame(settings);
            }
            catch (Exception e)
            {
                dispatcher.Invoke(() => viewModel.LogMessages.Add($"An error occured while starting the game: {e.Message}"));
            }
        }

        private async void PauseGame()
        {
            await Task.Run(() => Game.Pause());
        }

        private async void ResumeGame()
        {
            await Task.Run(() => Game.Resume());
        }

        private bool stopButtonEnabled = true;
        public bool StopButtonEnabled
        {
            get => stopButtonEnabled;
            set
            {
                SetValue(ref stopButtonEnabled, value);
            }
        }

        private async void StopGame()
        {
            StopButtonEnabled = false;

            await Task.Run(() => Game.StopGame());

            StopButtonEnabled = true;
        }

        private void QuitGame()
        {
            Application.Current.Shutdown();
        }

        private void SetupMap()
        {
            InSetup = !InSetup;
            ClearMapChange();
        }

        private void ClearMapChange()
        {
            foreach (var tile in ListAllTiles())
            {
                tile.IsClickable = false;
            }
        }

        private IEnumerable<TileViewModel> ListAllTiles()
        {
            return Map.MainMapGrid.Rows
                .Concat(Map.StartingMapGrid.Rows)
                .SelectMany(r => r.Tiles);
        }

        /// <summary>
        /// Needed to access the plugins window when refreshing
        /// </summary>
        public Plugins PluginsWindow = null;
        /// <summary>
        /// Needed in order to access the logger when scrolling
        /// </summary>
        public System.Windows.Controls.ListView Log = null;

        public void GameWindow_DataContextChanged()
        {
            LogMessages.CollectionChanged += (s, args) =>
            {
                ScrollLogToBottom();
            };

            Task.Run(() =>
            {
                var plugins = Lobby.GetAvailablePlugins();
                var plotterPlugin = plugins.Where(info => info.Name == "RoboRobo-Plotter").FirstOrDefault();
                if (plotterPlugin != null)
                    Lobby.AddPlugin(plotterPlugin);

                dispatcher.Invoke(() => PluginsWindow.RefreshPluginViews());
            });
        }

        /// <summary>
        /// Scroll the logger to the botom
        /// </summary>
        public void ScrollLogToBottom()
        {
            // copied from https://stackoverflow.com/a/18305272
            ListBoxAutomationPeer svAutomation = (ListBoxAutomationPeer)ScrollViewerAutomationPeer.CreatePeerForElement(Log);

            IScrollProvider scrollInterface = (IScrollProvider)svAutomation.GetPattern(PatternInterface.Scroll);
            System.Windows.Automation.ScrollAmount scrollVertical = System.Windows.Automation.ScrollAmount.LargeIncrement;
            System.Windows.Automation.ScrollAmount scrollHorizontal = System.Windows.Automation.ScrollAmount.NoAmount;
            //If the vertical scroller is not available, the operation cannot be performed, which will raise an exception. 
            if (scrollInterface.VerticallyScrollable)
                scrollInterface.Scroll(scrollHorizontal, scrollVertical);
        }


        private Visibility pluginsWindowVisibility = Visibility.Collapsed;
        public Visibility PluginsWindowVisibility
        {
            get => pluginsWindowVisibility;
            set
            {
                SetValue(ref pluginsWindowVisibility, value);
            }
        }

        private void OpenManagePluginsWindow()
        {
            PluginsWindowVisibility = Visibility.Visible;
        }
    }

    /// <summary>
    /// Helper class used for commands and their execution
    /// </summary>
    public class CommandHandler : ICommand
    {
        private Action action;
        private bool canExecute;
        /// <summary>
        /// Registers new command
        /// </summary>
        /// <param name="action"></param>
        /// <param name="canExecute"></param>
        public CommandHandler(Action action, bool canExecute)
        {
            this.action = action;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return canExecute;
        }

        /// <summary>
        /// not used
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            action();
        }
    }
}
