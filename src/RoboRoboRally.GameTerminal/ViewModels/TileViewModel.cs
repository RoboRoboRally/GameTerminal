﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB;
using System.Windows.Input;

namespace RoboRoboRally.GameTerminal.ViewModels
{
    /// <summary>
    /// ViewModel used for the TileView
    /// Handles buttons of the tile and values used to display the tile
    /// </summary>
    public class TileViewModel : BaseModel
    {
        private TileModel tile = null;
        public TileModel Tile
        {
            get { return tile; }
            set { SetValue(ref tile, value, nameof(Tile)); }
        }

        private bool isClickable = false;
        public bool IsClickable
        {
            get => isClickable;
            set => SetValue(ref isClickable, value);
        }

        private PlayerViewModel presentPlayer = null;
        /// <summary>
        /// Player that currently resides on this tile
        /// </summary>
        public PlayerViewModel PresentPlayer
        {
            get { return presentPlayer; }
            set { SetValue(ref presentPlayer, value, nameof(PresentPlayer)); }
        }

        GameWindowViewModel GameWindowViewModel;

        public TileViewModel(IMap map, int x, int y, GameWindowViewModel GameWindowViewModel)
        {
            this.GameWindowViewModel = GameWindowViewModel;
            Tile = new TileModel(map, x, y);
        }

        private ICommand leftButtonClickedCommand;
        public ICommand LeftButtonClickedCommand
        {
            get
            {
                return leftButtonClickedCommand ?? (leftButtonClickedCommand = new CommandHandler(() => LeftButtonClicked(), true));
            }
        }

        private ICommand rightButtonClickedCommand;
        public ICommand RightButtonClickedCommand
        {
            get
            {
                return rightButtonClickedCommand ?? (rightButtonClickedCommand = new CommandHandler(() => RightButtonClicked(), true));
            }
        }

        /// <summary>
        /// Used to assign a map element (radar, checkpoint ...) on a tile during game setup
        /// </summary>
        private void LeftButtonClicked()
        {
            GameWindowViewModel.TileModificationFunction(Tile);
            GameWindowViewModel.RefreshView();
        }

        /// <summary>
        /// Used to rotate a map element (radar, checkpoint ...) during game setup
        /// </summary>
        private void RightButtonClicked()
        {
            GameWindowViewModel.Map.RotateTile(Tile);            
        }
    }
}
