﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB.Tiles;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace RoboRoboRally.GameTerminal.ViewModels
{
    /// <summary>
    /// ViewModel for the GameSetupView
    /// Handles buttons used during the game setup, does not hold any data
    /// </summary>
    public class GameSetupViewModel : BaseModel
    {
        private MapsViewModel map;
        /// <summary>
        /// ViewModel for both maps in center
        /// </summary>
        public MapsViewModel Map
        {
            get => map;
            set => SetValue(ref map, value);
        }

        /// <summary>
        /// Pack of cards used during the game, can be changed in game setup
        /// </summary>
        public BindingList<ProgrammingCardCount> ProgrammingPack { get; set; }

        /// <summary>
        /// The option to use upgrades during the game
        /// </summary>
        public bool IncludeUpgrades
        {
            get => GameWindowViewModel.Settings.IncludeUpgrades;
            set
            {
                GameWindowViewModel.Settings.IncludeUpgrades = value;
                new Config.Config().IncludeUpgrades = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Needed for in order to use the ResetPackToDefault method
        /// </summary>
        public GameWindowViewModel GameWindowViewModel;

        public GameSetupViewModel(GameWindowViewModel GameWindowViewModel)
        {
            Map = GameWindowViewModel.Map;
            ProgrammingPack = GameWindowViewModel.ProgrammingPack;
            this.GameWindowViewModel = GameWindowViewModel;
        }

        private ICommand previousMainMapCommand;
        public ICommand PreviousMainMapCommand
        {
            get
            {
                return previousMainMapCommand ?? (previousMainMapCommand = new CommandHandler(() => PreviousMainMap(), true));
            }
        }

        private ICommand nextMainMapCommand;
        public ICommand NextMainMapCommand
        {
            get
            {
                return nextMainMapCommand ?? (nextMainMapCommand = new CommandHandler(() => NextMainMap(), true));
            }
        }

        private ICommand previousStartingMapCommand;
        public ICommand PreviousStartingMapCommand
        {
            get
            {
                return previousStartingMapCommand ?? (previousStartingMapCommand = new CommandHandler(() => PreviousStartingMap(), true));
            }
        }

        private ICommand nextStartingMapCommand;
        public ICommand NextStartingMapCommand
        {
            get
            {
                return nextStartingMapCommand ?? (nextStartingMapCommand = new CommandHandler(() => NextStartingMap(), true));
            }
        }

        private ICommand rotateMainMapCounterclockwiseCommand;
        public ICommand RotateMainMapCounterclockwiseCommand
        {
            get
            {
                return rotateMainMapCounterclockwiseCommand ?? (rotateMainMapCounterclockwiseCommand = new CommandHandler(() => RotateMainMapCounterclockwise(), true));
            }
        }

        private ICommand rotateMainMapClockwiseCommand;
        public ICommand RotateMainMapClockwiseCommand
        {
            get
            {
                return rotateMainMapClockwiseCommand ?? (rotateMainMapClockwiseCommand = new CommandHandler(() => RotateMainMapClockwise(), true));
            }
        }

        private ICommand eraseMapCommand;
        public ICommand EraseMapCommand
        {
            get
            {
                return eraseMapCommand ?? (eraseMapCommand = new CommandHandler(() => EraseMap(), true));
            }
        }

        private ICommand setAntennaCommand;
        public ICommand SetAntennaCommand
        {
            get
            {
                return setAntennaCommand ?? (setAntennaCommand = new CommandHandler(() => SetAntenna(), true));
            }
        }

        private ICommand toggleCheckpointCommand;
        public ICommand ToggleCheckpointCommand
        {
            get
            {
                return toggleCheckpointCommand ?? (toggleCheckpointCommand = new CommandHandler(() => ToggleCheckpoint(), true));
            }
        }

        private ICommand toggleRebootCommand;
        public ICommand ToggleRebootCommand
        {
            get
            {
                return toggleRebootCommand ?? (toggleRebootCommand = new CommandHandler(() => ToggleReboot(), true));
            }
        }

        private ICommand resetPackToDefaultCommand;
        public ICommand ResetPackToDefaultCommand
        {
            get
            {
                return resetPackToDefaultCommand ?? (resetPackToDefaultCommand = new CommandHandler(() => ResetPackToDefault(), true));
            }
        }

        private void PreviousMainMap() => Map.PreviousMainMap();

        private void NextMainMap() => Map.NextMainMap();

        private void PreviousStartingMap() => Map.PreviousStartingMap();

        private void NextStartingMap() => Map.NextStartingMap();

        private void RotateMainMapCounterclockwise() => Map.RotateMainMapCounterClockwise();

        private void RotateMainMapClockwise() => Map.RotateMainMapClockwise();

        private void EraseMap()
        {
            GameWindowViewModel.TileModificationFunction = _ => { };
            GameWindowViewModel.RefreshView = () => { };
            foreach (var tile in ListAllTiles())
            {
                tile.IsClickable = false;
            }
        }

        private void SetAntenna()
        {
            GameWindowViewModel.TileModificationFunction =
                Map.SetAntenna;
            GameWindowViewModel.RefreshView = () => SetAntenna();
            foreach (var tile in ListAllTiles())
            {
                var modelTile = GetModelTile(tile);
                tile.IsClickable = modelTile is EmptyTile
                    || modelTile is PriorityAntennaTile;
            }
        }

        private void ToggleCheckpoint()
        {
            GameWindowViewModel.TileModificationFunction = Map.ToggleCheckpoint;
            GameWindowViewModel.RefreshView = () => ToggleCheckpoint();

            foreach (var tile in ListAllTiles())
            {
                var modelTile = GetModelTile(tile);
                tile.IsClickable = modelTile is EmptyTile
                    || modelTile is CheckpointTile;
            }
        }

        private void ToggleReboot()
        {
            GameWindowViewModel.TileModificationFunction = Map.ToggleReboot;
            GameWindowViewModel.RefreshView = () => ToggleReboot();

            foreach (var tile in ListAllTiles())
            {
                var modelTile = GetModelTile(tile);
                tile.IsClickable = modelTile is EmptyTile
                    || modelTile is RebootTile;
            }
        }

        private Tile GetModelTile(TileViewModel vm)
        {
            return vm.Tile.GetModelTile();
        }

        private void ResetPackToDefault()
        {
            GameWindowViewModel.ResetPackToDefault();
        }

        private IEnumerable<TileViewModel> ListAllTiles()
        {
            return Map.MainMapGrid.Rows
                .Concat(Map.StartingMapGrid.Rows)
                .SelectMany(r => r.Tiles);
        }
    }
}
