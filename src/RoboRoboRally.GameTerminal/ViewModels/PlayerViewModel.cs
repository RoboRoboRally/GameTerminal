﻿using Newtonsoft.Json;
using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.ViewModels
{
    /// <summary>
    /// ViewModel for the PlayerView
    /// Holds information about the player used to display cards, batteries and checkpoints and handles the KickPlayer button
    /// </summary>
    public class PlayerViewModel : BaseModel
    {
        private GameWindowViewModel gameWindow;

        public ObservableCollection<SlotModel> TemporaryUpgrades { get; set; } = new ObservableCollection<SlotModel>();
        public ObservableCollection<SlotModel> ProgrammingCards { get; set; } = new ObservableCollection<SlotModel>();
        public ObservableCollection<SlotModel> PermanentUpgrades { get; set; } = new ObservableCollection<SlotModel>();

        public RobotInfo Robot { get; set; }
        /// <summary>
        /// Color of programming cards used - one color per robot
        /// </summary>
        public Color Color => (Color)ColorConverter.ConvertFromString(Robot.Color);

        private string backgroundColor = Constants.backgroundColor;
        /// <summary>
        /// Background color - the same as robot player
        /// Used to higlight player when playing a card
        /// </summary>
        public string BackgroundColor
        {
            get => backgroundColor;
            set => SetValue(ref backgroundColor, value);
        }

        /// <summary>
        /// Position and direction of players robot
        /// </summary>
        public MapCoordinates coordinates = new MapCoordinates(0, 0);
        /// <summary>
        /// Direction which players robot is facing
        /// </summary>
        public DirectionType direction = DirectionType.Up;

        public bool QrScreenSelected => !IsPicked && !Won;
        public bool MainScreenSelected => IsPicked && !Won;
        public bool FinishedScreenSeelcted => Won;

        private bool damageTaken = false;
        /// <summary>
        /// Used to show the damage highlight
        /// </summary>
        public bool DamageTaken
        {
            get => damageTaken;
            set => SetValue(ref damageTaken, value);
        }

        private bool upgradeUsed = false;
        /// <summary>
        /// Used to show the upgrade highlight
        /// </summary>
        public bool UpgradeUsed
        {
            get => upgradeUsed;
            set => SetValue(ref upgradeUsed, value);
        }

        private bool isPicked = false;
        /// <summary>
        /// Indicates if the robot was picked - used to show or hide QR code
        /// </summary>
        public bool IsPicked
        {
            get => isPicked;
            set => SetValue(ref isPicked, value);
        }

        private string qrContent;
        /// <summary>
        /// Content of QR code, which player needs to screen in order to connect
        /// </summary>
        public string QRContent
        {
            get { return qrContent; }
            set { SetValue(ref qrContent, value, nameof(QRContent)); }
        }

        private int batteryCount = 5;
        /// <summary>
        /// Used to show the number of batteries
        /// </summary>
        public int BatteryCount
        {
            get => batteryCount;
            set => SetValue(ref batteryCount, value);
        }

        private int lastPassedCheckpoint = 0;
        /// <summary>
        /// Used to show the checkpoints passed
        /// </summary>
        public int LastPassedCheckpoint
        {
            get => lastPassedCheckpoint;
            set => SetValue(ref lastPassedCheckpoint, value);
        }

        private bool won = false;
        /// <summary>
        /// Used to indicate, if the victory screen should be displayed
        /// </summary>
        public bool Won
        {
            get => won;
            set => SetValue(ref won, value);
        }

        public PlayerViewModel(GameWindowViewModel gameWindow, RobotInfo robot, PlayerQrConnectionData connectionData)
        {
            this.gameWindow = gameWindow;
            Robot = robot;

            QRContent = JsonConvert.SerializeObject(connectionData);

            for (int i = 0; i < 3; i++)
            {
                TemporaryUpgrades.Add(new SlotModel(robot.Color, new CardModel(Constants.upgradeCardBack)));
                PermanentUpgrades.Add(new SlotModel(robot.Color, new CardModel(Constants.upgradeCardBack)));
            }

            for (int i = 0; i < 5; i++)
            {
                ProgrammingCards.Add(new SlotModel(robot.Color, new CardModel(Constants.programmingCardBack)));
            }

            PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == nameof(Won)
                    || args.PropertyName == nameof(IsPicked))
                {
                    OnPropertyChanged(nameof(QrScreenSelected));
                    OnPropertyChanged(nameof(MainScreenSelected));
                    OnPropertyChanged(nameof(FinishedScreenSeelcted));
                }
            };
        }

        /// <summary>
        /// Clean all players data as a preparation for a new game
        /// </summary>
        public void Clean()
        {
            for (int i = 0; i < 3; i++)
            {
                TemporaryUpgrades[i].Card.CardId = Constants.upgradeCardBack;
                PermanentUpgrades[i].Card.CardId = Constants.upgradeCardBack;
            }

            for (int i = 0; i < 5; i++)
            {
                ProgrammingCards[i].Card.CardId = Constants.programmingCardBack;
            }

            BatteryCount = 5;
            LastPassedCheckpoint = 0;
            DamageTaken = false;
            UpgradeUsed = false;
            Won = false;
        }

        private ICommand kickPlayerCommand;
        public ICommand KickPlayerCommand
        {
            get
            {
                return kickPlayerCommand ?? (kickPlayerCommand = new CommandHandler(() => KickPlayer(), true));
            }
        }

        /// <summary>
        /// Used for the kick button, disconnect player.
        /// Server will issue calls, which will remove players robot from the map and show qr code later
        /// </summary>
        public async void KickPlayer()
        {
            var connections = await Task.Run(() => gameWindow.Lobby.GetAllConnections());

            foreach (var connection in connections)
            {
                if (connection.SelectedRobot?.Name == Robot.Name)
                {
                    await Task.Run(() => gameWindow.Lobby.RemoveConnection(connection));
                    return;
                }
            }
        }
    }
}