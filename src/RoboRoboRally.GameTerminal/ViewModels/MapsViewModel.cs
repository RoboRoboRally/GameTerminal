﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace RoboRoboRally.GameTerminal.ViewModels
{
    /// <summary>
    /// ViewModel for the MapView
    /// Handles the map selection and rotation logic and draws the laser rays
    /// Holds currently selected main and starting map
    /// </summary>
    public class MapsViewModel : BaseModel
    {
        private MapFactory factory = new MapFactory();

        //current map
        //map grid
        IList<IMap> mainMaps = null;
        IList<IMap> startingMaps = null;

        Dictionary<int, RotationType> mapRotations = new Dictionary<int, RotationType>();

        /// <summary>
        /// Used to disable / enable start button
        /// </summary>
        public bool SettingsValid
        {
            get
            {
                var mapSettings = CreateSettings();
                return mapSettings.PriorityAntenna != null && mapSettings.RebootTiles?.Length > 0 && mapSettings.CheckpointTiles?.Length > 0;
            }
        }

        private int selectedMainMapId;
        private int SelectedMainMapId
        {
            get => selectedMainMapId;
            set
            {
                selectedMainMapId = value;
                SelectedMainMap = mainMaps[value];
            }
        }

        private int selectedStartingMapId;
        private int SelectedStartingMapId
        {
            get => selectedStartingMapId;
            set
            {
                selectedStartingMapId = value;
                SelectedStartingMap = startingMaps[value];
            }
        }

        private IMap selectedMainMap = null;
        /// <summary>
        /// Instance of the IMap from Model representing main map
        /// Used to change the position of antena, checkpoint and reboot tile
        /// </summary>
        public IMap SelectedMainMap
        {
            get => selectedMainMap;
            set
            {
                SetValue(ref selectedMainMap, value);
                MainMapGrid = new MapGrid(value, GameWindowViewModel);
            }
        }

        private IMap selectedStartingMap = null;
        /// <summary>
        /// Instance of the IMap from Model representing starting map
        /// Used to change the position of antena, checkpoint and reboot tile
        /// </summary>
        public IMap SelectedStartingMap
        {
            get => selectedStartingMap;
            set
            {
                SetValue(ref selectedStartingMap, value);
                StartingMapGrid = new MapGrid(value, GameWindowViewModel);
            }
        }

        /// <summary>
        /// Used as a grid of TileViewModels for the individual tiles
        /// </summary>
        private MapGrid mainMapGrid = null;
        /// <summary>
        /// Used to represent the main map as a grid of TileViewModels
        /// </summary>
        public MapGrid MainMapGrid
        {
            get { return mainMapGrid; }
            set { SetValue(ref mainMapGrid, value); }
        }

        private MapGrid startingMapGrid = null;
        /// <summary>
        /// Used to represent the main map as a grid of TileViewModels
        /// </summary>
        public MapGrid StartingMapGrid
        {
            get { return startingMapGrid; }
            set { SetValue(ref startingMapGrid, value); }
        }

        GameWindowViewModel GameWindowViewModel;

        /// <summary>
        /// Load all available maps and select one as a main and starting map
        /// Add default positions of antena, checkpoint and reboot tile
        /// </summary>
        /// <param name="GameWindowViewModel"></param>
        public MapsViewModel(GameWindowViewModel GameWindowViewModel)
        {
            this.GameWindowViewModel = GameWindowViewModel;

            Config.Config config = new Config.Config();
            string databasePath = Path.Combine(config.AbsoluteGameDataFolder, "DB/MapDB/");
            
            //Loads the maps and selects one
            mainMaps = new MapLoader(databasePath).LoadGamePlans();
            SelectedMainMapId = 0;
            for (int i = 0; i < mainMaps.Count; ++i)
            {
                mapRotations.Add(i, RotationType.None);

                if (mainMaps[i].ID == "5B")
                    SelectedMainMapId = i;
            }

            //Loads the maps and selects one
            startingMaps = new MapLoader(databasePath).LoadStartingPlans();
            SelectedStartingMapId = 0;
            for (int i = 0; i < startingMaps.Count; ++i)
            {
                if (startingMaps[i].ID == "A")
                    SelectedStartingMapId = i;
            }

            // Preprepared setup of the available maps
            SetAntenna(mainMaps[0], new MapCoordinates(0, 5));
            AddReboot(mainMaps[0], new MapCoordinates(0, 9));
            AddCheckpoint(mainMaps[0], new MapCoordinates(0, 4));

            SetAntenna(mainMaps[1], new MapCoordinates(5, 5));
            AddReboot(mainMaps[1], new MapCoordinates(6, 5));
            AddCheckpoint(mainMaps[1], new MapCoordinates(0, 5));

            RefreshRays();
        }

        /// <summary>
        /// Creates map setting which are send to the server
        /// </summary>
        /// <returns></returns>
        public MapSettings CreateSettings()
        {
            var map = factory.JoinMaps(SelectedMainMap, SelectedStartingMap);

            return new MapSettings
            {
                GamePlanId = SelectedMainMap.ID,
                StartingPlanId = SelectedStartingMap.ID,
                GamePlanRotation = mapRotations[SelectedMainMapId],
                PriorityAntenna = map.GetTileRecords<PriorityAntennaTile>().Select(t => new OrientedMapCoordinates
                {
                    Coordinates = t.Coords,
                    Direction = t.Tile.OutDirection
                }).First(),
                CheckpointTiles = map.GetTileRecords<CheckpointTile>().OrderBy(t => t.Tile.Number).Select(t => t.Coords).ToArray(),
                RebootTiles = map.GetTileRecords<RebootTile>().Select(t => new OrientedMapCoordinates
                {
                    Coordinates = t.Coords,
                    Direction = t.Tile.OutDirection
                }).ToArray()
            };
        }

        public void RotateMainMapCounterClockwise()
        {
            var newRotation = (RotationType)(((int)mapRotations[SelectedMainMapId] + (int)RotationType.Left) % 4);
            mapRotations[SelectedMainMapId] = newRotation;
            SelectedMainMap = factory.RotateMap(SelectedMainMap, RotationType.Left);
            RefreshRays();
        }

        public void RotateMainMapClockwise()
        {
            var newRotation = (RotationType)(((int)mapRotations[SelectedMainMapId] + (int)RotationType.Right) % 4);
            mapRotations[SelectedMainMapId] = newRotation;
            SelectedMainMap = factory.RotateMap(SelectedMainMap, RotationType.Right);
            RefreshRays();
        }

        public void NextMainMap()
        {
            SelectedMainMapId = (SelectedMainMapId + 1) % mainMaps.Count;
            RefreshMaps();
        }

        public void PreviousMainMap()
        {
            SelectedMainMapId = SelectedMainMapId == 0 ? mainMaps.Count - 1 : SelectedMainMapId - 1;
            RefreshMaps();
        }

        public void NextStartingMap()
        {
            SelectedStartingMapId = (SelectedStartingMapId + 1) % startingMaps.Count;
            RefreshMaps();
        }

        public void PreviousStartingMap()
        {
            SelectedStartingMapId = SelectedStartingMapId == 0 ? startingMaps.Count - 1 : SelectedStartingMapId - 1;
            RefreshMaps();
        }

        /// <summary>
        /// If the tile which was clicked is Antena or a Reboot tile, rotate it
        /// </summary>
        /// <param name="model"></param>
        public void RotateTile(TileModel model)
        {
            if (model.Map != SelectedMainMap && model.Map != SelectedStartingMap)
                return;

            var modelTile = model.GetModelTile();
            var mapCoords = new MapCoordinates(model.Y, model.X);
            var fullCoords = CoordsFromMapSpecificToFull(model.Map, mapCoords);

            if (modelTile is RebootTile reboot)
            {
                var newDirection = reboot.OutDirection.Rotate(RotationType.Right);

                model.Map.ClearSetTile(mapCoords);
                model.Map.SetRebootTile(new RebootTile(newDirection), mapCoords);
            }
            else if (modelTile is PriorityAntennaTile antenna)
            {
                var newDirection = antenna.OutDirection.Rotate(RotationType.Right);
                model.Map.ClearSetTile(mapCoords);
                model.Map.SetPriorityAntennaTile(new PriorityAntennaTile(newDirection), mapCoords);
            }
            else return;

            RefreshMaps();
        }

        /// <summary>
        /// If the tile is empty, assign a checkpoint
        /// If the tile already has a checkpoint remove it
        /// </summary>
        /// <param name="model"></param>
        public void ToggleCheckpoint(TileModel model)
        {
            if (model.Map != SelectedMainMap && model.Map != SelectedStartingMap)
                return;

            var modelTile = model.GetModelTile();

            if (modelTile is CheckpointTile)
            {
                RemoveCheckpoint(model.Map, new MapCoordinates(model.Y, model.X));
            }
            else
            {
                AddCheckpoint(model.Map, new MapCoordinates(model.Y, model.X));
            }
        }

        private void AddCheckpoint(IMap map, MapCoordinates mapCoordinates)
        {
            var checkpoints = GetTiles<CheckpointTile>();
            var nextNum = checkpoints.Count + 1;

            map.SetCheckpointTile(new CheckpointTile(nextNum), mapCoordinates);

            RefreshMaps();
        }

        /// <summary>
        /// Removes the checkpoint and modifies text of the previously added checkpoints
        /// </summary>
        /// <param name="map"></param>
        /// <param name="mapCoordinates">position of removed checkpoint</param>
        private void RemoveCheckpoint(IMap map, MapCoordinates mapCoordinates)
        {
            var checkpoints = GetTiles<CheckpointTile>();

            if (checkpoints.Count == 1)
                return;// will not delete last checkpoint

            checkpoints.Sort((c1, c2) => c1.Item2.Tile.Number.CompareTo(c2.Item2.Tile.Number));

            var tile = map[mapCoordinates] as CheckpointTile;

            for (int i = tile.Number - 1; i < checkpoints.Count; ++i)
                checkpoints[i].Item1.ClearSetTile(checkpoints[i].Item2.Coords);

            for (int i = tile.Number; i < checkpoints.Count; ++i)
                checkpoints[i].Item1.SetCheckpointTile(new CheckpointTile(i), checkpoints[i].Item2.Coords);

            RefreshMaps();
        }

        /// <summary>
        /// Adds a new antena on the tile
        /// </summary>
        /// <param name="model"></param>
        public void SetAntenna(TileModel model)
        {
            if (model.Map != SelectedMainMap && model.Map != SelectedStartingMap)
                return;

            var newCoords = new MapCoordinates(model.Y, model.X);

            SetAntenna(model.Map, newCoords);
        }

        /// <summary>
        /// Removes previous Antena, adds new to to new position
        /// </summary>
        /// <param name="map"></param>
        /// <param name="coords">position for new antena</param>
        private void SetAntenna(IMap map, MapCoordinates coords)
        {
            var antennaTiles = GetTiles<PriorityAntennaTile>();
            var direction = antennaTiles.Count > 0 ? antennaTiles[0].Item2.Tile.OutDirection : new Direction(DirectionType.Up);

            foreach (var tile in antennaTiles)
                tile.Item1.ClearSetTile(tile.Item2.Coords);

            map.SetPriorityAntennaTile(new PriorityAntennaTile(direction), coords);

            RefreshMaps();
        }

        /// <summary>
        /// If the tile is empty, assign a reboot tile
        /// If the tile already has a reboot remove it
        /// </summary>
        /// <param name="model"></param>
        public void ToggleReboot(TileModel model)
        {
            if (model.Map != SelectedMainMap && model.Map != SelectedStartingMap)
                return;

            var modelTile = model.GetModelTile();

            if (modelTile is RebootTile)
            {
                RemoveReboot(model.Map, new MapCoordinates(model.Y, model.X));
            }
            else
            {
                AddReboot(model.Map, new MapCoordinates(model.Y, model.X));
            }
        }

        private void RemoveReboot(IMap map, MapCoordinates mapCoordinates)
        {
            var rebootTiles = GetTiles<RebootTile>();

            if (rebootTiles.Count == 1)
                return;

            map.ClearSetTile(mapCoordinates);

            RefreshMaps();
        }

        private void AddReboot(IMap map, MapCoordinates mapCoordinates)
        {
            map.SetRebootTile(new RebootTile(new Direction(DirectionType.Up)), mapCoordinates);

            RefreshMaps();
        }

        private List<Tuple<IMap, TileRecord<T>>> GetTiles<T>() where T : Tile
        {
            var mainMapTiles = SelectedMainMap.GetTileRecords<T>().Select(t => new Tuple<IMap, TileRecord<T>>(SelectedMainMap, t));
            var startingMapTiles = SelectedStartingMap.GetTileRecords<T>().Select(t => new Tuple<IMap, TileRecord<T>>(SelectedStartingMap, t));

            return mainMapTiles.Concat(startingMapTiles).ToList();
        }

        private MapCoordinates CoordsFromMapSpecificToFull(IMap map, MapCoordinates coords)
        {
            if (map == SelectedMainMap)
                return coords;
            else
                return new MapCoordinates(
                    coords.RowIndex + SelectedMainMap.MapSize.RowCount,
                    coords.ColIndex + SelectedMainMap.MapSize.ColCount);
        }


        private void RefreshMaps()
        {
            SelectedMainMap = SelectedMainMap; // to invoke setters
            SelectedStartingMap = SelectedStartingMap;
            RefreshRays();

            OnPropertyChanged(nameof(SettingsValid));
        }

        /// <summary>
        /// Redraws all laser rays
        /// </summary>
        /// <param name="player">Optionaly draw lasers of a player</param>
        public void RefreshRays(PlayerViewModel player = null)
        {
            RefreshRays(MainMapGrid, StartingMapGrid, player);
        }

        private void RefreshRays(MapGrid mainMap, MapGrid startMap, PlayerViewModel player = null)
        {
            ClearRays(mainMap);
            ClearRays(startMap);
            DrawLaserRays(mainMap);
            DrawLaserRays(startMap);

            if (player == null)
            {
                return;
            }

            DrawPlayerRays(mainMap, startMap, player);
        }

        // ---- Map Lasers ----

        /// <summary>
        /// Go throught all lasers in on the map, find whech way are the oriented, draw them
        /// Draw until you reach the end of map or until you hit a player
        /// </summary>
        /// <param name="map"></param>
        private void DrawLaserRays(MapGrid map)
        {
            LaserPath[] mainMapRays = map.map.GetLaserWallLaserPaths();
            foreach (var ray in mainMapRays)
            {
                switch (ray.LaserDirection)
                {
                    case DirectionType.Right:
                        TraceRightLaser(ray, map);
                        break;
                    case DirectionType.Up:
                        TraceUpLaser(ray, map);
                        break;
                    case DirectionType.Left:
                        TraceLeftLaser(ray, map);
                        break;
                    case DirectionType.Down:
                        TraceDownLaser(ray, map);
                        break;
                    default:
                        break;
                }
            }
        }

        private void TraceRightLaser(LaserPath ray, MapGrid map)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;
            try
            {
                while (currentColumn != ray.End.ColIndex && map.Rows[currentRow].Tiles[currentColumn].PresentPlayer == null)
                {
                    map.Rows[currentRow].Tiles[currentColumn].Tile.HorizontalRays += ray.LaserCount;
                    currentColumn++;
                }
            }
            catch (Exception) { }
        }

        private void TraceUpLaser(LaserPath ray, MapGrid map)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;

            try
            {
                while (currentRow != ray.End.RowIndex && map.Rows[currentRow].Tiles[currentColumn].PresentPlayer == null)
                {
                    map.Rows[currentRow].Tiles[currentColumn].Tile.VerticalRays += ray.LaserCount;
                    currentRow--;
                }
            }
            catch (Exception) { }
        }

        private void TraceLeftLaser(LaserPath ray, MapGrid map)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;

            try
            {
                while (currentColumn != ray.End.ColIndex && map.Rows[currentRow].Tiles[currentColumn].PresentPlayer == null)
                {
                    map.Rows[currentRow].Tiles[currentColumn].Tile.HorizontalRays += ray.LaserCount;
                    currentColumn--;
                }
            }
            catch (Exception) { }
        }

        private void TraceDownLaser(LaserPath ray, MapGrid map)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;

            try
            {
                while (currentRow != ray.End.RowIndex && map.Rows[currentRow].Tiles[currentColumn].PresentPlayer == null)
                {
                    map.Rows[currentRow].Tiles[currentColumn].Tile.VerticalRays += ray.LaserCount;
                    currentRow++;
                }
            }
            catch (Exception) { }
        }

        private void ClearRays(MapGrid map)
        {
            foreach (var Row in map.Rows)
            {
                foreach (var TileModel in Row.Tiles)
                {
                    TileModel.Tile.HorizontalRays = 0;
                    TileModel.Tile.VerticalRays = 0;
                }
            }
        }

        // ---- Player Lasers ----

        /// <summary>
        /// Create a laser for a player, draw the laser in a direction which the player is facing
        /// If the player has certain upgrades, which modifies the laser behaviour, apply them
        /// RailGun will make the laser pass through all obstacles
        /// With RearLaser robot will shoot also in the oposite direction
        /// </summary>
        /// <param name="mainMap"></param>
        /// <param name="startMap"></param>
        /// <param name="player"></param>
        private void DrawPlayerRays(MapGrid mainMap, MapGrid startMap, PlayerViewModel player)
        {
            LaserPath playerRay = new LaserPath() { LaserCount = 1, Start = player.coordinates, LaserDirection = player.direction };

            bool railGun = player.PermanentUpgrades.Any(i => i.Card.CardId == Constants.railGun);
            bool rearLaser = player.PermanentUpgrades.Any(i => i.Card.CardId == Constants.rearLaser);   
            
            int maxCol = SelectedMainMap.MapSize.ColCount;
            int maxRow = SelectedMainMap.MapSize.RowCount + SelectedStartingMap.MapSize.RowCount;

            switch (player.direction)
            {
                case DirectionType.Right:
                    playerRay.End = new MapCoordinates(player.coordinates.RowIndex, maxCol);
                    TraceRightPlayer(playerRay, mainMap, startMap, railGun);
                    if (rearLaser)
                    {
                        playerRay.End = new MapCoordinates(player.coordinates.RowIndex, -1);
                        TraceLeftPlayer(playerRay, mainMap, startMap, railGun);
                    }
                    break;
                case DirectionType.Up:
                    playerRay.End = new MapCoordinates(-1, player.coordinates.ColIndex);
                    TraceUpPlayer(playerRay, mainMap, startMap, railGun);
                    if (rearLaser)
                    {
                        playerRay.End = new MapCoordinates(maxRow, player.coordinates.ColIndex);
                        TraceDownPlayer(playerRay, mainMap, startMap, railGun);
                    }
                    break;
                case DirectionType.Left:
                    playerRay.End = new MapCoordinates(player.coordinates.RowIndex, -1);
                    TraceLeftPlayer(playerRay, mainMap, startMap, railGun);
                    if (rearLaser)
                    {
                        playerRay.End = new MapCoordinates(player.coordinates.RowIndex, maxCol);
                        TraceRightPlayer(playerRay, mainMap, startMap, railGun);
                    }
                    break;
                case DirectionType.Down:
                    playerRay.End = new MapCoordinates(maxRow, player.coordinates.ColIndex);
                    TraceDownPlayer(playerRay, mainMap, startMap, railGun);
                    if (rearLaser)
                    {
                        playerRay.End = new MapCoordinates(-1, player.coordinates.ColIndex);
                        TraceUpPlayer(playerRay, mainMap, startMap, railGun);
                    }
                    break;
                default:
                    break;
            }
        }

        private bool PlayerOnTile(MapGrid mainMap, MapGrid startMap, int currentRow, int currentColumn)
        {
            if (currentRow <= (SelectedMainMap.MapSize.ColCount-1))
            {
                if (mainMap.Rows[currentRow].Tiles[currentColumn].PresentPlayer == null)
                {
                    return false;
                }
                return true;
            }
            else
            {
                if (startMap.Rows[currentRow - SelectedMainMap.MapSize.ColCount].Tiles[currentColumn].PresentPlayer == null)
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Used to put player rays into the map
        /// Simply increases the number of rays on a tile in given direction
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="mainMap"></param>
        /// <param name="startMap"></param>
        /// <param name="currentRow"></param>
        /// <param name="currentColumn"></param>
        private void AssignRay(LaserPath ray, MapGrid mainMap, MapGrid startMap, int currentRow, int currentColumn)
        {
            if (currentRow <= SelectedMainMap.MapSize.ColCount - 1)
            {
                if (ray.LaserDirection == DirectionType.Down || ray.LaserDirection == DirectionType.Up)
                {
                    mainMap.Rows[currentRow].Tiles[currentColumn].Tile.VerticalRays += ray.LaserCount;
                }
                else
                {
                    mainMap.Rows[currentRow].Tiles[currentColumn].Tile.HorizontalRays += ray.LaserCount;
                }
            }
            else
            {
                if (ray.LaserDirection == DirectionType.Down || ray.LaserDirection == DirectionType.Up)
                {
                    startMap.Rows[currentRow - SelectedMainMap.MapSize.ColCount].Tiles[currentColumn].Tile.VerticalRays += ray.LaserCount;
                }
                else
                {
                    startMap.Rows[currentRow - SelectedMainMap.MapSize.ColCount].Tiles[currentColumn].Tile.HorizontalRays += ray.LaserCount;
                }
            }
        }

        private void TraceRightPlayer(LaserPath ray, MapGrid mainMap, MapGrid startMap, bool railGun)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;
            // Offset in order not to stop thanks to our own robot
            currentColumn++;
            IMap mergedMap = (new MapFactory()).JoinMaps(mainMap.map, startMap.map);

            try
            {
                while (railGun || (currentColumn != ray.End.ColIndex && !PlayerOnTile(mainMap, startMap, currentRow, currentColumn)))
                {
                    if (!railGun && mergedMap.IsWallBetween(new MapCoordinates(currentRow, currentColumn - 1), new MapCoordinates(currentRow, currentColumn)))
                    {
                        break;
                    }
                    AssignRay(ray, mainMap, startMap, currentRow, currentColumn);
                    currentColumn++;
                }
            }
            catch (Exception) { }
        }

        private void TraceUpPlayer(LaserPath ray, MapGrid mainMap, MapGrid startMap, bool railGun)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;
            // Offset in order not to stop thanks to our own robot
            currentRow--;
            IMap mergedMap = (new MapFactory()).JoinMaps(mainMap.map, startMap.map);

            try
            {
                while (railGun || (currentRow != ray.End.RowIndex && !PlayerOnTile(mainMap, startMap, currentRow, currentColumn)))
                {
                    if (!railGun && mergedMap.IsWallBetween(new MapCoordinates(currentRow + 1, currentColumn), new MapCoordinates(currentRow, currentColumn)))
                    {
                        break;
                    }
                    AssignRay(ray, mainMap, startMap, currentRow, currentColumn);
                    currentRow--;
                }
            }
            catch (Exception) { }
        }

        private void TraceLeftPlayer(LaserPath ray, MapGrid mainMap, MapGrid startMap, bool railGun)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;
            // Offset in order not to stop thanks to our own robot
            currentColumn--;
            IMap mergedMap = (new MapFactory()).JoinMaps(mainMap.map, startMap.map);

            try
            {
                while (railGun || (currentColumn != ray.End.ColIndex && !PlayerOnTile(mainMap, startMap, currentRow, currentColumn)))
                {
                    if (!railGun && mergedMap.IsWallBetween(new MapCoordinates(currentRow, currentColumn + 1), new MapCoordinates(currentRow, currentColumn)))
                    {
                        break;
                    }
                    AssignRay(ray, mainMap, startMap, currentRow, currentColumn);
                    currentColumn--;
                }
            }
            catch (Exception) { }
        }

        private void TraceDownPlayer(LaserPath ray, MapGrid mainMap, MapGrid startMap, bool railGun)
        {
            int currentColumn = ray.Start.ColIndex;
            int currentRow = ray.Start.RowIndex;
            // Offset in order not to stop thanks to our own robot
            currentRow++;
            IMap mergedMap = (new MapFactory()).JoinMaps(mainMap.map, startMap.map);

            try
            {
                while (railGun || (currentRow != ray.End.RowIndex && !PlayerOnTile(mainMap, startMap, currentRow, currentColumn)))
                {
                    if (!railGun && mergedMap.IsWallBetween(new MapCoordinates(currentRow - 1, currentColumn), new MapCoordinates(currentRow, currentColumn)))
                    {
                        break;
                    }
                    AssignRay(ray, mainMap, startMap, currentRow, currentColumn);
                    currentRow++;
                }
            }
            catch (Exception) { }
        }
    }

    /// <summary>
    /// Used to create map of TileViewModels for individual tiles
    /// </summary>
    public class MapGrid
    {
        public ObservableCollection<TileRow> Rows { get; set; } = new ObservableCollection<TileRow>();

        public IMap map = null;

        public MapGrid(IMap map, GameWindowViewModel GameWindowViewModel)
        {
            this.map = map;
            for (int i = 0; i < map.MapSize.RowCount; ++i)
            {
                Rows.Add(new TileRow(map, i, GameWindowViewModel));
            }
        }
    }

    /// <summary>
    /// Used to create map of TileViewModels for individual tiles
    /// </summary>
    public class TileRow
    {
        public ObservableCollection<TileViewModel> Tiles { get; set; } = new ObservableCollection<TileViewModel>();

        public TileRow(IMap map, int y, GameWindowViewModel GameWindowViewModel)
        {
            for (int x = 0; x < 10; x++)
            {
                Tiles.Add(new TileViewModel(map, x, y, GameWindowViewModel));
            }
        }
    }
}