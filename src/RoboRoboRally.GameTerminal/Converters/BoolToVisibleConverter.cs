﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts boolean to visibility (true = Visible)
    /// </summary>
    class BoolToVisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool bValue = (bool)value;
            if (bValue)
                return Visibility.Visible;
            else
                return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;

            return visibility == Visibility.Visible;
        }
    }
}
