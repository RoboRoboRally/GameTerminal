﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Model.Textures.RobotTextures;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts PlayerViewModel into ImageSourcce of a robot texture
    /// For the conversion we need player name to get the texture and player direction in order to rotate the texture properly
    /// Used to display the robot texture on game board
    /// </summary>
    class PlayerViewModelToImageSourceConverter : IValueConverter
    {
        RobotTextureLoader loader;
        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        Config.Config config = new Config.Config();

        public PlayerViewModelToImageSourceConverter()
        {
            string path = Path.Combine(config.AbsoluteGameDataFolder, "Textures/RobotTextures");
            loader = new RobotTextureLoader(path);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            try
            {
                PlayerViewModel PresentPlayer = (PlayerViewModel)value;
                return PlayerToImageSource(PresentPlayer);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private ImageSource PlayerToImageSource(PlayerViewModel PresentPlayer)
        {
            Bitmap bitmap;
            bitmap = (Bitmap)tc.ConvertFrom(loader.GetRobotTopViewTexture(PresentPlayer.Robot.Name));
            RotateFlipType rotate = ImageTransformer.GetRotationFlipTypeFromDirectionType(PresentPlayer.direction);
            bitmap.RotateFlip(rotate);
            return ImageTransformer.BitmapToImageSource(bitmap);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
