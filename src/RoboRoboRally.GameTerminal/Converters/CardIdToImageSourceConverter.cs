﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.Textures.CardTextures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts the programming card ID into an ImageSource
    /// Once displayed values are cached in order to save loading times
    /// Used to display programming cards
    /// </summary>
    class CardIdToImageSourceConverter : IMultiValueConverter
    {
        CardTextureLoader loader;
        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        Config.Config config = new Config.Config();
        static Dictionary<string, BitmapImage> cache = new Dictionary<string, BitmapImage>();

        public CardIdToImageSourceConverter()
        {
            string path = Path.Combine(config.AbsoluteGameDataFolder, "Textures/CardTextures");
            loader = new CardTextureLoader(path);
        }

        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value[0] == null || value[1] == null)
            {
                return null;
            }

            try
            {
                return LoadTexture(value[0] as string, value[1] as string);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private BitmapImage LoadTexture(string programmingCardName, string playerColor)
        {
            var cacheKey = $"{programmingCardName}/{playerColor}";
            if (cache.ContainsKey(cacheKey))
                return cache[cacheKey];

            Bitmap bitmap;
            if (programmingCardName == "ProgrammingCardBack")
            {
                bitmap = (Bitmap)tc.ConvertFrom(loader.GetProgrammingCardBackTexture(playerColor));
            }
            else
            {
                bitmap = (Bitmap)tc.ConvertFrom(loader.GetProgrammingCardTexture(programmingCardName, playerColor));
            }
            var image = ImageTransformer.BitmapToImageSource(bitmap);

            cache.Add(cacheKey, image);
            return image;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
