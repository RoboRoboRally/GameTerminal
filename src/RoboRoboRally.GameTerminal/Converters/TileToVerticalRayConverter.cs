﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter used for drawing the laser rays of vertical lasers
    /// Returns SolidColorBrush, which is used to draw on a rectangle in the view
    /// Parameter indicates if the texture is intended for center (value 1) or outer (value 2) lasers  and is set in the view
    /// Depending on the number of lasers and position of laser we decide, if we should draw this laser red
    /// </summary>
    class TileToVerticalRayConverter : IValueConverter
    {
        public TileToVerticalRayConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            try
            {
                int VerticalRays = (int)value;
                int type = (int)parameter;
                if ((type == 1 && (VerticalRays == 1 || VerticalRays == 3)) || (type == 2 && (VerticalRays == 2 || VerticalRays == 3)))
                {
                    return new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 0));
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
