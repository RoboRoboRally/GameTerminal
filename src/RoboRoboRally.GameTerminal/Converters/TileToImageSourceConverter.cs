﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.Textures.MapTextures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter used to convert TieModel into ImageSource of a tile texture
    /// Once converted tiles are cached in order to reduce loading times
    /// The tile textures are rotated and flipped in order to cover all variations
    /// </summary>
    class TileToImageSourceConverter : IValueConverter
    {
        /// <summary>
        /// Comparer for the tile texture caching
        /// </summary>
        class TileTextureInfoComparer : EqualityComparer<TileTextureInfo>
        {
            public override bool Equals(TileTextureInfo x, TileTextureInfo y)
            {
                return x.Texture == y.Texture && x.RotationEnumValue == y.RotationEnumValue && x.FlipEnumValue == y.FlipEnumValue;
            }

            public override int GetHashCode(TileTextureInfo obj)
            {
                return obj.Texture.GetHashCode() + obj.FlipEnumValue.GetHashCode() + obj.RotationEnumValue.GetHashCode();
            }
        }

        MapTextureLoader loader;
        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        Config.Config config = new Config.Config();
        static Dictionary<TileTextureInfo, BitmapImage> cache = new Dictionary<TileTextureInfo, BitmapImage>(new TileTextureInfoComparer());

        public TileToImageSourceConverter()
        {
            string path = Path.Combine(config.AbsoluteGameDataFolder, "Textures/MapTextures");
            loader = new MapTextureLoader(path);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            try
            {
                TileModel tileModel = (TileModel)value;
                string databasePath = Path.Combine(config.AbsoluteGameDataFolder, "DB/MapDB/");
                TileTextureInfo tileInfo = loader.GetTileTexture(tileModel.Map, tileModel.Y, tileModel.X);
                return TileTextureInfoToImageSource(tileInfo);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private ImageSource TileTextureInfoToImageSource(TileTextureInfo tileInfo)
        {
            if (cache.ContainsKey(tileInfo))
                return cache[tileInfo];

            Bitmap bitmap;
            bitmap = (Bitmap)tc.ConvertFrom(tileInfo.Texture);
            RotateFlipType rotate = (RotateFlipType)tileInfo.RotationEnumValue;
            RotateFlipType flip = (RotateFlipType)tileInfo.FlipEnumValue;
            bitmap.RotateFlip(rotate);
            bitmap.RotateFlip(flip);
            var result = ImageTransformer.BitmapToImageSource(bitmap);

            cache.Add(tileInfo, result);
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
