﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.Textures.RobotTextures;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts robot name into a ImageSource
    /// Used for the robot image near QR code
    /// </summary>
    class RobotNameToImageSourceConverter : IValueConverter
    {
        RobotTextureLoader loader;
        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        Config.Config config = new Config.Config();

        public RobotNameToImageSourceConverter()
        {
            string path = Path.Combine(config.AbsoluteGameDataFolder, "Textures/RobotTextures");
            loader = new RobotTextureLoader(path);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            try
            {
                return NameToImageSource((string)value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private ImageSource NameToImageSource(string name)
        {
            Bitmap bitmap;
            bitmap = (Bitmap)tc.ConvertFrom(loader.GetRobotFrontViewTexture(name));
            return ImageTransformer.BitmapToImageSource(bitmap);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
