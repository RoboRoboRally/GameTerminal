﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts TileModel into a string
    /// Used for text on the tiles, such as number of checkpoint or map ID
    /// </summary>
    class TileToTextConverter : IValueConverter
    {
        public TileToTextConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            try
            {
                TileModel tileModel = (TileModel)value;
                IMap map = tileModel.Map;
                Tile tile = map[new MapCoordinates((int)tileModel.Y, (int)tileModel.X)];
                if (tile is TextTile textTile)
                {
                    return textTile.Text;
                }
                else if (tile is CheckpointTile checkpointTile)
                {
                    return checkpointTile.Number.ToString();
                }

                return string.Empty;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
