﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Textures.MapTextures;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts the TileModel into a wall texture ImageSource
    /// LaserWall and PushPanelWall both should have wall bellow the laser or push panel
    /// The parameter signalises the side of tile square, which we are interrested in (set in the view)
    /// </summary>
    class TileToEmptyWallImageSourceConverter : IValueConverter
    {
        MapTextureLoader loader;
        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        Config.Config config = new Config.Config();

        public TileToEmptyWallImageSourceConverter()
        {
            string path = Path.Combine(config.AbsoluteGameDataFolder, "Textures/MapTextures");
            loader = new MapTextureLoader(path);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            try
            {
                TileModel tileModel = (TileModel)value;
                IMap map = tileModel.Map;
                Tile tile = map[new MapCoordinates((int)tileModel.Y, (int)tileModel.X)];
                var walls = map.GetTileWalls(tile);
                foreach (var wall in walls)
                {
                    if (wall is EmptyWall || wall is LaserWall || wall is PushPanelWall)
                    {
                        if (wall.WallLocation == (WallLocation)(int)parameter)
                        {
                            return EmptyWallToImageSource(wall);
                        }
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private ImageSource EmptyWallToImageSource(Wall emptyWall)
        {
            Bitmap bitmap;
            bitmap = (Bitmap)tc.ConvertFrom(loader.GetEmptyWallTexture());
            RotateFlipType rotate = ImageTransformer.GetRotationFlipTypeFromDirectionType(emptyWall.FacingDirection.GetDirectionType());
            bitmap.RotateFlip(rotate);
            return ImageTransformer.BitmapToImageSource(bitmap);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
