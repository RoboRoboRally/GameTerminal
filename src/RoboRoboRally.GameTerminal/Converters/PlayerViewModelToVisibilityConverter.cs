﻿using RoboRoboRally.GameTerminal.ViewModels;
using System;
using System.Globalization;
using System.Windows.Data;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts PlayerViewModel to a string indicating visibility
    /// Used for a Rectangle singnalising damage or upgrade usage
    /// The parameter indicates if given rectanhle signalises damage or upgrade
    /// </summary>
    class PlayerViewModelToVisibilityConverter : IValueConverter
    {
        public PlayerViewModelToVisibilityConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return "Hidden";
            }

            try
            {                
                PlayerViewModel player = (PlayerViewModel)value;
                bool trigger = false;                
                if ((int)parameter == 0)
                {
                    trigger = player.DamageTaken;
                }

                if ((int)parameter == 1)
                {
                    trigger = player.UpgradeUsed;
                }

                if (trigger == true)
                {
                    return "Visible";
                }
                else
                {
                    return "Hidden";
                }
            }
            catch (Exception)
            {
                return "Hidden";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
