﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Textures.MapTextures;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts TileModel into a push panel texture ImageSource
    /// The parameter signalises the side of tile square, which we are interrested in (set in the view)
    /// </summary>
    class TileToPushPanelImageSourceConvertor : IValueConverter
    {
        MapTextureLoader loader;
        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        Config.Config config = new Config.Config();

        public TileToPushPanelImageSourceConvertor()
        {
            string path = Path.Combine(config.AbsoluteGameDataFolder, "Textures/MapTextures/");
            loader = new MapTextureLoader(path);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            try
            {
                TileModel tileModel = (TileModel)value;
                IMap map = tileModel.Map;
                Tile tile = map[tileModel.Y, tileModel.X];
                var walls = map.GetTileWalls(tile);
                foreach (var wall in walls)
                {
                    if (wall is PushPanelWall pushPanelWall)
                    {
                        if (pushPanelWall.WallLocation == (WallLocation)(int)parameter)
                        {
                            return PushPanelToImageSource(pushPanelWall);
                        }
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private ImageSource PushPanelToImageSource(PushPanelWall pushPanelWall)
        {
            Bitmap bitmap;
            bitmap = (Bitmap)tc.ConvertFrom(loader.GetPushPanelTexture(pushPanelWall));
            RotateFlipType rotate = ImageTransformer.GetRotationFlipTypeFromDirectionType(pushPanelWall.FacingDirection.GetDirectionType());
            bitmap.RotateFlip(rotate);
            return ImageTransformer.BitmapToImageSource(bitmap);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
