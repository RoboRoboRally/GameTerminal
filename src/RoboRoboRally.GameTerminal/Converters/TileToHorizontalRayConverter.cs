﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter used for drawing the laser rays of horizontal lasers
    /// Returns SolidColorBrush, which is used to draw on a rectangle in the view
    /// Parameter indicates if the texture is intended for center (value 1) or outer (value 2) lasers and is set in the view
    /// Depending on the number of lasers and position of laser we decide, if we should draw this laser
    /// </summary>
    class TileToHorizontalRayConverter : IValueConverter
    {
        public TileToHorizontalRayConverter() { }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            try
            {
                int HorizontalRays = (int)value;
                int type = (int)parameter;
                // If type is 1 (center laser) and there are 1 or 3 lasers OR type is 2 (edge lasers) and there are 2 or 3 lasers
                if ((type == 1 && (HorizontalRays == 1 || HorizontalRays == 3)) || (type == 2 && (HorizontalRays == 2 || HorizontalRays == 3)))
                {
                    return new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 0));
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
