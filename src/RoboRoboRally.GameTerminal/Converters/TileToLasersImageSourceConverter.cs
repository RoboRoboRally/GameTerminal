﻿using RoboRoboRally.GameTerminal.Models;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Textures.MapTextures;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter used to convert TileModel into a laser head texture ImageSource
    /// Parameter indicates if the texture is intended for center (value 1) or outer (value 2) lasers  and is set in the view
    /// Depending on the number of lasers and position of laser we decide, if we should draw this laser
    /// </summary>
    class TileToLasersImageSourceConverter : IMultiValueConverter
    {
        MapTextureLoader loader;
        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
        Config.Config config = new Config.Config();

        public TileToLasersImageSourceConverter()
        {
            string path = Path.Combine(config.AbsoluteGameDataFolder, "Textures/MapTextures");
            loader = new MapTextureLoader(path);
        }

        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value[0] == null || value[1] == null)
            {
                return null;
            }

            try
            {
                TileModel tileModel = (TileModel)value[0];
                IMap map = tileModel.Map;
                Tile tile = map[new MapCoordinates((int)tileModel.Y, (int)tileModel.X)];
                var walls = map.GetTileWalls(tile);
                foreach (var wall in walls)
                {
                    if (wall is LaserWall laserWall)
                    {
                        if (laserWall.WallLocation == (WallLocation)(int)value[1])
                        {
                            return LaserWallToImageSource(laserWall, (int)parameter);
                        }
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private ImageSource LaserWallToImageSource(LaserWall laserWall, int type)
        {
            // If type is 1 (center laser) and there are 1 or 3 lasers OR type is 2 (edge lasers) and there are 2 or 3 lasers
            if ((type == 1 && (laserWall.Count == 1 || laserWall.Count == 3)) || (type == 2 && (laserWall.Count == 2 || laserWall.Count == 3)))
            {
                Bitmap bitmap;
                bitmap = (Bitmap)tc.ConvertFrom(loader.GetLaserHeadTexture());
                RotateFlipType rotate = ImageTransformer.GetRotationFlipTypeFromDirectionType(laserWall.FacingDirection.GetDirectionType());
                bitmap.RotateFlip(rotate);
                return ImageTransformer.BitmapToImageSource(bitmap);
            }
            else
            {
                return null;
            }
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
