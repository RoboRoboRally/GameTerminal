﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace RoboRoboRally.GameTerminal.Converters
{
    /// <summary>
    /// Converter which converts a string into a QR code encoding the string
    /// We use the ZXing library for this purpose    .
    /// </summary>
    class StringToQRConverter : IValueConverter
    {
        public StringToQRConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.ToString() == "")
            {
                return null;
            }

            QRCodeWriter qrcode = new QRCodeWriter();
            BarcodeWriter barcodeWriter = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new EncodingOptions
                {
                    Width = 500,
                    Height = 500,
                    Margin = 0
                }
            };

            using (Bitmap bitmap = barcodeWriter.Write(value.ToString()))
            {
                IntPtr hbmp = bitmap.GetHbitmap();
                BitmapSource source = Imaging.CreateBitmapSourceFromHBitmap(
                hbmp,
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
                return source;

            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
