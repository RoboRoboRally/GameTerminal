﻿using RoboRoboRally.GameTerminal.Properties;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Threading;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator informs us about map elements activity
    /// Utilised only for notification on the log and time delay for events
    /// Used in the activation phase
    /// </summary>
    class MapElementSpectator : IMapElementSpectator
    {
        private Action<object> onReceivedMessage;

        public MapElementSpectator(Action<object> onReceivedMessage)
        {
            this.onReceivedMessage = onReceivedMessage;
        }

        public void BlueBeltsActivated()
        {
            //onReceivedMessage(Resources.BlueBeltsActivated);
            // Time is needed for players to see the change
            Thread.Sleep(1000);
        }

        public void CheckpointsActivated()
        {
            //onReceivedMessage(Resources.CheckpointsActivated);
        }

        public void EnergySpacesActivated()
        {
            //onReceivedMessage(Resources.EnergySpacesActivated);
        }

        public void GameStarted(IGame game)
        {
        }

        public void GreenBeltsActivated()
        {
            //onReceivedMessage(Resources.GreenBeltsActivated);
            // Time is needed for players to see the change
            Thread.Sleep(1000);
        }

        public void MapElementActivationStarted()
        {
            onReceivedMessage(Resources.MapElementsActivating);
        }

        public void PushPanelsActivated()
        {
            //onReceivedMessage(Resources.PushPanelsActivated);
        }

        public void RotatingGearsActivated()
        {
            //onReceivedMessage(Resources.RotatingGearsActivated);
        }

        public void WallLasersActivated()
        {
            //onReceivedMessage(Resources.WallLasersActivated);
        }
    }
}
