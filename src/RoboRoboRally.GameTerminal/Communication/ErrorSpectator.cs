﻿using RoboRoboRally.Server.Interfaces;
using System;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator is only used to inform players about an issue
    /// Used across all phases
    /// </summary>
    class ErrorSpectator : IErrorSpectator
    {
        private Action<object> onReceivedMessage;

        public ErrorSpectator(Action<object> onReceivedMessage)
        {
            this.onReceivedMessage = onReceivedMessage;
        }

        public void FatalRunningGameError(string message)
        {
            onReceivedMessage(string.Format("Fatal error: {0}", message));
        }

        public void FatalUnexpectedError(string message)
        {
            onReceivedMessage(string.Format("Fatal error: {0}", message));
        }

        public void GameStarted(IGame game)
        {
        }
    }
}
