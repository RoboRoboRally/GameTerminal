﻿using RoboRoboRally.GameTerminal.Properties;
using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Linq;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator handles events such as start of the game, end of the game and phases
    /// Used across all phases
    /// </summary>
    class GameFlowSpectator : IGameFlowSpectator
    {
        //public event Action OnGameOver;
        private Action<object> onReceivedMessage;
        public IGame Game { get; private set; }
        private GameWindowViewModel GameVM;

        public GameFlowSpectator(Action<object> onReceivedMessage, GameWindowViewModel GameVM)
        {
            this.onReceivedMessage = onReceivedMessage;
            this.GameVM = GameVM;
        }

        public void TurnStarted(int turnNumber)
        {
            onReceivedMessage(string.Format(Resources.TurnStarted, turnNumber));
        }

        public void TurnEnded(int turnNumber)
        {
            //onReceivedMessage(string.Format(Resources.TurnEnded, turnNumber));
            GameVM.CleanProgrammingCards();
        }

        public void PhaseStarted(string phaseName)
        {
            onReceivedMessage(string.Format(Resources.PhaseStarted, phaseName)); //TODO localise phase names
        }

        public void PhaseEnded(string phaseName)
        {
            //onReceivedMessage(string.Format(Resources.PhaseEnded, phaseName));
        }

        public void GamePaused()
        {
            onReceivedMessage(Resources.GamePaused);
            GameVM.IsGamePaused = true;
        }

        public void GameResumed()
        {
            onReceivedMessage(Resources.GameResumed);
            GameVM.IsGamePaused = false;
        }

        /// <summary>
        /// After an end of the game, we have to manualy remove all players from the board
        /// </summary>
        public void GameOver()
        {
            onReceivedMessage(Resources.GameOver);
            GameVM.IsGameRunning = false;
            foreach (var player in GameVM.Players)
            {
                GameVM.RemovePlayerPosition(player);
                player.Won = false;
            }
        }

        public void GameIsEnding()
        {
            onReceivedMessage(Resources.GameIsEnding);
        }

        /// <summary>
        /// After finishing the game, remove player from the board and show victory screen
        /// </summary>
        /// <param name="robot"></param>
        public void RobotFinishedGame(RobotInfo robot)
        {
            onReceivedMessage(string.Format(Resources.RobotFinishedGame, robot.Name));
            var playerVM = GameVM.Players.Single(p => p.Robot.Name == robot.Name);
            playerVM.Won = true;
            GameVM.RemovePlayerPosition(playerVM);
        }

        /// <summary>
        /// Used to count checkpoints of a player
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="checkpointNr"></param>
        public void RobotVisitedCheckpoint(RobotInfo robot, int checkpointNr)
        {
            onReceivedMessage(string.Format(Resources.RobotVisitedCheckpoint, robot.Name, checkpointNr));
            GameVM.RobotVisitedCheckpoint(robot, checkpointNr);
        }

        public void RobotPriorityDetermined(RobotInfo[] robotsInOrder)
        {
            string robots = string.Join(", ", robotsInOrder.Select(r => r.Name));
            onReceivedMessage(string.Format(Resources.RobotPriorityDetermined, robots));
        }

        /// <summary>
        /// Initialises new game and resets information from previous game
        /// </summary>
        /// <param name="game"></param>
        public void GameStarted(IGame game)
        {
            Game = game;
            onReceivedMessage(Resources.GameStarted);
            GameVM.IsGameRunning = true;
            GameVM.Reset();
        }
    }
}
