﻿using RoboRoboRally.GameTerminal.Properties;
using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Interfaces;
using System;


namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator informs us about the movement of robots
    /// Used in activation phase
    /// </summary>
    class MovementSpectator : IMovementSpectator
    {
        private Action<object> onReceivedMessage;
        private GameWindowViewModel GameVM;

        public MovementSpectator(Action<object> onReceivedMessage, GameWindowViewModel GameVM)
        {
            this.onReceivedMessage = onReceivedMessage;
            this.GameVM = GameVM;
        }

        public void GameStarted(IGame game)
        {
        }

        /// <summary>
        /// Robot joined the game
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="coordinates">His initial position on the map</param>
        /// <param name="direction">His initial direction</param>
        public void RobotAdded(RobotInfo robot, MapCoordinates coordinates, Direction direction)
        {
            var dir = GetDirectionTranslated(direction);

            // Sometimes robot did not show, duplicate is there just to make sure
            GameVM.RelocateRobot(robot, coordinates, direction);
            GameVM.RelocateRobot(robot, coordinates, direction);

            onReceivedMessage(string.Format(Resources.RobotAdded,
                robot.Name, coordinates.ColIndex, coordinates.RowIndex, dir));
        }

        /// <summary>
        /// Robot moved using programming card or an upgrade
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="newCoords"></param>
        public void RobotMoved(RobotInfo robot, MapCoordinates newCoords)
        {
            GameVM.RelocateRobot(robot, newCoords);
            //onReceivedMessage(string.Format(Resources.RobotMoved, robot.Name, newCoords.ColIndex, newCoords.RowIndex));
        }

        /// <summary>
        /// Robot moved by being pushed by other robot
        /// </summary>
        /// <param name="robot">pushed robot</param>
        /// <param name="oldCoords">old position - not used</param>
        /// <param name="newCoords">new postition</param>
        public void RobotPushed(RobotInfo robot, MapCoordinates oldCoords, MapCoordinates newCoords)
        {
            GameVM.RelocateRobot(robot, newCoords);

            onReceivedMessage(string.Format(Resources.RobotPushed, robot.Name, newCoords.ColIndex, newCoords.RowIndex));
        }

        /// <summary>
        /// Robot rebooted and is moved to a reboot tile
        /// After relocation mark the rest of his cards as unusable
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="mapCoordinates">new position</param>
        /// <param name="direction"> new direction</param>
        public void RobotRebooting(RobotInfo robot, MapCoordinates mapCoordinates, Direction direction)
        {
            GameVM.RelocateRobot(robot, mapCoordinates, direction);
            GameVM.Reboot(robot);

            onReceivedMessage(string.Format(Resources.RobotRebooting, robot.Name, mapCoordinates.ColIndex, mapCoordinates.RowIndex));
        }

        /// <summary>
        /// Robot removed from the game board, for example after kick
        /// </summary>
        /// <param name="robot"></param>
        public void RobotRemoved(RobotInfo robot)
        {
            GameVM.RemoveRobot(robot);
            //onReceivedMessage(string.Format(Resources.RobotRemoved, robot.Name));
        }

        /// <summary>
        /// Robot rotated using programming cards or an upgrade
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="rotation"></param>
        public void RobotRotated(RobotInfo robot, RotationType rotation)
        {
            GameVM.RotateRobot(robot, rotation);

            /*string rot = "N/A";
            switch (rotation)
            {
                case RotationType.Left:
                    rot = Resources.RotationLeft;
                    break;
                case RotationType.Right:
                    rot = Resources.RotationRight;
                    break;
                case RotationType.None:
                    rot = Resources.RotationNone;
                    break;
                case RotationType.Back:
                    rot = Resources.RotationBack;
                    break;
            }
            onReceivedMessage(string.Format(Resources.RobotTurned, robot.Name, rot));*/
        }

        /// <summary>
        /// Robot teleported to a new position. Similar to RobotRebooted but is triggered by different game event
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="newCoords">new position</param>
        /// <param name="direction">new direction</param>
        public void RobotTeleported(RobotInfo robot, MapCoordinates newCoords, Direction direction)
        {
            GameVM.RelocateRobot(robot, newCoords, direction);

            var dir = GetDirectionTranslated(direction);
            onReceivedMessage(string.Format(Resources.RobotTeleported, robot.Name, newCoords.ColIndex, newCoords.RowIndex, dir));
        }

        private string GetDirectionTranslated(Direction direction)
        {
            string dir = "N/A";
            switch (direction.GetDirectionType())
            {
                case DirectionType.Up:
                    dir = Resources.DirectionUp;
                    break;
                case DirectionType.Down:
                    dir = Resources.DirectionDown;
                    break;
                case DirectionType.Left:
                    dir = Resources.DirectionLeft;
                    break;
                case DirectionType.Right:
                    dir = Resources.DirectionRight;
                    break;
            }

            return dir;
        }
    }


}
