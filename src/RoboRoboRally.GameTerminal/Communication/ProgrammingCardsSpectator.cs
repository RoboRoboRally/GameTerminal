﻿using RoboRoboRally.GameTerminal.Properties;
using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Threading;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator informs us about various events connected to programming cards
    /// Used in both programming and activation phase
    /// </summary>
    class ProgrammingCardsSpectator : IProgrammingCardSpectator
    {
        private Action<object> onReceivedMessage;
        private GameWindowViewModel GameVM;

        public ProgrammingCardsSpectator(Action<object> onReceivedMessage, GameWindowViewModel GameVM)
        {
            this.onReceivedMessage = onReceivedMessage;
            this.GameVM = GameVM;
        }

        /// <summary>
        /// Used to highlight cards of a player with a given robot.
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="card"></param>
        public void CardBeingPlayed(RobotInfo robot, string card)
        {
            //onReceivedMessage(string.Format(Resources.CardBeingPlayed, robot.Name, card));      
            GameVM.HighlightCards(robot);
            Thread.Sleep(2000);
            GameVM.UnHighlightCards(robot);
        }

        public void CardBurned(RobotInfo robot, string card)
        {
            //onReceivedMessage(string.Format(Resources.CardBurned, robot.Name, card));
        }

        /// <summary>
        /// Replaces a revealed programming card in the card line
        /// For example spam card is replaced by the card being played
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="register">In which register is the card changed</param>
        /// <param name="oldCard">not used</param>
        /// <param name="newCard"></param>
        public void RevealedCardReplaced(RobotInfo robot, int register, CardInfo oldCard, CardInfo newCard)
        {
            onReceivedMessage(string.Format(Resources.CardReplaced, robot.Name, oldCard.CardId, register, newCard.CardId));
            GameVM.CardRevealed(robot, register, newCard.CardId);
        }

        /// <summary>
        /// Used at the begging of each turn. Shows the next card in register
        /// </summary>
        /// <param name="revealedCard"></param>
        public void CardRevealed(RevealedProgrammingCard revealedCard)
        {
            /*onReceivedMessage(string.Format(Resources.CardRevealed, 
                revealedCard.RevealedCard, revealedCard.Register, revealedCard.Robot.Name));*/

            GameVM.CardRevealed(revealedCard.Robot, revealedCard.Register, revealedCard.RevealedCard);
        }

        public void CardsDealt()
        {
            onReceivedMessage(Resources.CardsDealt);
        }

        public void CardSelectionOver()
        {
            onReceivedMessage(Resources.CardSelectionOver);
        }

        public void CardsSelected(RobotInfo robot, int count)
        {
            //onReceivedMessage(string.Format(Resources.CardsSelected, robot.Name, count));
        }

        public void CountdownStarted(DateTime countdownEnd)
        {
        }

        /// <summary>
        /// When a robot receives damage cards (spam, virus ...) we receive this call
        /// Used to highligh the robot on game board - robot is shown in red overlay
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="damage"></param>
        public void DamageCardsTaken(RobotInfo robot, RoboRoboRally.Server.Interfaces.Damage damage)
        {
            GameVM.DamageSignal(robot);
            onReceivedMessage(string.Format(Resources.DamageCardsTaken, robot.Name, string.Join(", ", damage.DamageCards)));
            Thread.Sleep(750);
            GameVM.DamageUnsignal(robot);
        }

        public void GameStarted(IGame game)
        {
        }

        public void RegisterActivated(int register)
        {
            onReceivedMessage(string.Format(Resources.RegisterActivated, register));
        }

        public void CardsNotSelectedOnTime(RobotInfo robot)
        {
        }
    }
}
