﻿using RoboRoboRally.GameTerminal.Properties;
using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Threading;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator informs us about various events connected to upgrade cards
    /// Used during the buy phase and activation phase
    /// </summary>
    class UpgradeCardsSpectator : IUpgradeCardSpectator
    {
        private Action<object> onReceivedMessage;
        private GameWindowViewModel GameVM;

        public UpgradeCardsSpectator(Action<object> onReceivedMessage, GameWindowViewModel GameVM)
        {
            this.onReceivedMessage = onReceivedMessage;
            this.GameVM = GameVM;
        }

        /// <summary>
        /// If the player discards a card, exchange this card with a new one
        /// Otherwise just add the upgrade card
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="boughtCard"></param>
        public void CardBought(RobotInfo robot, PurchaseResponse boughtCard)
        {
            if (boughtCard.SelectedCard == null && boughtCard.DiscardedCard == null)
            {
                onReceivedMessage(string.Format(Resources.NoCardBought,
                    robot.Name));
                return;
            }

            if (boughtCard.DiscardedCard == null)
            {
                onReceivedMessage(string.Format(Resources.CardBought,
                    robot.Name, boughtCard.SelectedCard.CardId));
            }
            else
            {
                onReceivedMessage(string.Format(Resources.CardBought,
                    robot.Name, boughtCard.SelectedCard.CardId));
                onReceivedMessage(string.Format(Resources.DiscardedCard,
                    robot.Name, boughtCard.DiscardedCard.CardId));
            }

            GameVM.CardBought(robot, boughtCard);
        }

        public void CardsOfferedInShop(RobotInfo robot, PurchaseOffer offer)
        {

        }

        public void GameStarted(IGame game)
        {
        }

        public void NoCardBought(RobotInfo robot)
        {
            onReceivedMessage(string.Format(Resources.NoCardBought,
                robot.Name));
        }

        public void UpgradeCardNotUsed(RobotInfo robot)
        {
        }

        public void UpgradeCardOffered(RobotInfo robot, CardInfo cardInfo)
        {
        }

        /// <summary>
        /// This call is issued when a player uses and upgrade card (both passive and temporary)
        /// If the upgrade is temporary, remove it from the screen
        /// Also highlight the player on the game board - robot is shown in yellow overlay
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="cardSelection"></param>
        public void UpgradeCardUsed(RobotInfo robot, CardInfo cardSelection)
        {
            GameVM.UpgradeSignal(robot, cardSelection);
            onReceivedMessage(string.Format(Resources.UpgradeCardUsed,
                robot.Name, cardSelection.CardId));
            Thread.Sleep(750);
            GameVM.UpgradeUnsignal(robot);
        }
    }
}
