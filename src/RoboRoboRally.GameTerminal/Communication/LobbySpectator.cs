﻿using RoboRoboRally.GameTerminal.Properties;
using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator informs us about the lobby activity
    /// Used across all phases
    /// </summary>
    class LobbySpectator : ILobbySpectator
    {
        private Action<object> onReceivedMessage;
        GameWindowViewModel GameVM;

        public LobbySpectator(Action<object> onReceivedMessage, GameWindowViewModel GameVM)
        {
            this.onReceivedMessage = onReceivedMessage;
            this.GameVM = GameVM;
        }

        /// <summary>
        /// A player or an AI chose to use a robot
        /// Hide the robot QR code and show his cards
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="robot"></param>
        public void RobotPicked(string clientName, RobotInfo robot)
        {
            onReceivedMessage(string.Format(Resources.RobotPicked, clientName, robot.Name));
            GameVM.RobotPicked(robot);
        }

        /// <summary>
        /// A player or an AI left the game and freed a robot
        /// Hide the robot cards and show his QR code
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="robot"></param>
        public void RobotFreed(string clientName, RobotInfo robot)
        {
            onReceivedMessage(string.Format(Resources.RobotFreed, clientName, robot.Name));
            GameVM.RobotFreed(robot);
        }

        public void ClientJoined(string clientName)
        {
            onReceivedMessage(string.Format(Resources.ClientJoined, clientName));
        }

        public void ClientLeft(string clientName)
        {
            onReceivedMessage(string.Format(Resources.ClientLeft, clientName));
        }

        public void LobbyClosing()
        {
            onReceivedMessage(Resources.LobbyClosing);
        }

        public void LobbyClosed()
        {
            onReceivedMessage(Resources.LobbyClosed);
        }

        public void GameStarted(IGame game)
        {

        }
    }
}
