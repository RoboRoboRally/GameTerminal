﻿using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Core.Connections;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// Class representing connection between terminal and the server
    /// </summary>
    class TerminalConnection : BaseConnection
    {
        public GameFlowSpectator GameFlowSpectator;
        public IGame Game => GameFlowSpectator.Game;

        Dictionary<Type, IClient> Spectators = new Dictionary<Type, IClient>();

        /// <summary>
        /// Register the various spectators on the server
        /// </summary>
        /// <param name="onReceivedMessage"></param>
        /// <param name="GameVM"></param>
        public TerminalConnection(Action<object> onReceivedMessage, GameWindowViewModel GameVM)
        {
            GameFlowSpectator = new GameFlowSpectator(onReceivedMessage, GameVM);
            AddSpectator<IGameFlowSpectator>(GameFlowSpectator);
            AddSpectator<ILobbySpectator>(new LobbySpectator(onReceivedMessage, GameVM));
            AddSpectator<IMapElementSpectator>(new MapElementSpectator(onReceivedMessage));
            AddSpectator<IMovementSpectator>(new MovementSpectator(onReceivedMessage, GameVM));
            AddSpectator<IProgrammingCardSpectator>(new ProgrammingCardsSpectator(onReceivedMessage, GameVM));
            AddSpectator<IRobotSpectator>(new RobotSpectator(onReceivedMessage, GameVM));
            AddSpectator<IUpgradeCardSpectator>(new UpgradeCardsSpectator(onReceivedMessage, GameVM));
            AddSpectator<IErrorSpectator>(new ErrorSpectator(onReceivedMessage));
        }

        public override T GetClient<T>()
        {
            return (T)Spectators[typeof(T)];
        }

        public override bool SupportsClient<T>()
        {
            return Spectators.ContainsKey(typeof(T));
        }

        public void AddSpectator<TClient>(TClient client)
            where TClient : IClient
        {
            Spectators.Add(typeof(TClient), client);
        }

    }
}
