﻿using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Threading;

namespace RoboRoboRally.GameTerminal.Communication
{
    /// <summary>
    /// This spectator informs us about changes in the robots state
    /// Used during the activation phase
    /// </summary>
    class RobotSpectator : IRobotSpectator
    {
        private Action<object> onReceivedMessage;
        private GameWindowViewModel GameVM;

        public RobotSpectator(Action<object> onReceivedMessage, GameWindowViewModel GameVM)
        {
            this.onReceivedMessage = onReceivedMessage;
            this.GameVM = GameVM;
        }

        public void GameStarted(IGame game)
        {
        }

        public void RobotActivated(RobotState robotState)
        {
        }

        public void RobotDeactivated(RobotInfo robot)
        {
        }

        /// <summary>
        /// Update of robots batteries
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="oldEnergy">not used</param>
        /// <param name="newEnergy"></param>
        public void RobotEnergyChanged(RobotInfo robot, int oldEnergy, int newEnergy)
        {
            GameVM.BatteriesChanged(robot, newEnergy);
        }

        /// <summary>
        /// Used to draw an laser ray of the player
        /// The wait will delay new calls from the server, which gives the players enough time to see the laser
        /// </summary>
        /// <param name="robot"></param>
        public void RobotLasersActivated(RobotInfo robot)
        {
            GameVM.RobotLasers(robot, true);
            //onReceivedMessage(string.Format(Resources.RobotLasersActivated, robot.Name));
            Thread.Sleep(750);
            GameVM.RobotLasers(robot, false);
        }
    }
}
