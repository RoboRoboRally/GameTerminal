﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace RoboRoboRally.GameTerminal.Config
{
    /// <summary>
    /// Used to configure some options of the game
    /// Options include timeouts for various actions (some will be displayed on the mobile application),
    /// usage of upgrades and pack of programming cards
    /// </summary>
    internal class Config : IConfig, Server.Core.IConfig
    {
        public ushort Port => GetConfigValue<ushort>();

        public bool UseHttps => GetConfigValue<bool>();

        public string AbsoluteGameDataFolder => AppDomain.CurrentDomain.BaseDirectory + "/" + GetConfigValue<string>("GameDataFolder");

        public IEnumerable<string> PluginFolders => GetConfigValue<string>().Split(';');

        public TimeSpan ProgrammingTimeout => TimeSpan.FromSeconds(
            GetConfigValue<double>("ProgrammingTimeoutInSeconds"));

        public TimeSpan NetworkLatencyAcceptedDelay => TimeSpan.FromMilliseconds(
            GetConfigValue<double>("NetworkLatencyAcceptedDelayInMilliseconds"));

        public TimeSpan UpgradeCardBuyTimeout => TimeSpan.FromSeconds(
            GetConfigValue<double>("UpgradeCardBuyTimeoutInSeconds"));

        public TimeSpan UpgradeCardUseTimeout => TimeSpan.FromSeconds(
            GetConfigValue<double>("UpgradeCardUseTimeoutInSeconds"));

        public TimeSpan AsyncCardOfferProcessTimeout => TimeSpan.FromSeconds(
            GetConfigValue<double>("AsyncCardOfferProcessTimeoutInSeconds"));

        public bool IncludeUpgrades
        {
            get => Properties.Settings.Default.IncludeUpgrades;
            set
            {
                Properties.Settings.Default.IncludeUpgrades = value;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// The initial pack of cards
        /// </summary>
        public List<KeyValuePair<string, int>> ProgrammingPackDefaults
        {
            get => new List<KeyValuePair<string, int>>
            {
                new KeyValuePair<string, int>( "MoveBack", 1),
                new KeyValuePair<string, int>( "Move1", 5),
                new KeyValuePair<string, int>( "Move2", 3),
                new KeyValuePair<string, int>( "Move3", 1),
                new KeyValuePair<string, int>( "Again", 2),
                new KeyValuePair<string, int>( "PowerUp", 1),
                new KeyValuePair<string, int>( "UTurn", 1),
                new KeyValuePair<string, int>( "LeftTurn", 3),
                new KeyValuePair<string, int>( "RightTurn", 3),
            };
        }

        public List<KeyValuePair<string, int>> ProgrammingPackLastUsed
        {
            get => JsonConvert.DeserializeObject<List<KeyValuePair<string, int>>>(Properties.Settings.Default.ProgrammingPackLastUsed);
            set
            {
                Properties.Settings.Default.ProgrammingPackLastUsed = JsonConvert.SerializeObject(value);
                Properties.Settings.Default.Save();
            }
        }

        public int MinProgrammingCardCount => GetConfigValue<int>();

        private T GetConfigValue<T>([CallerMemberName]string keyName = null)
        {
            string value = ConfigurationManager.AppSettings[keyName];

            return (T)Convert.ChangeType(value, typeof(T));
        }

        private void SetConfigValue<T>(T item, [CallerMemberName]string keyName = null)
        {
            ConfigurationManager.AppSettings[keyName] = item.ToString();
        }
    }
}
