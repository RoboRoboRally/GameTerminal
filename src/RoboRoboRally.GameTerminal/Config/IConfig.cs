﻿using System.Collections.Generic;

namespace RoboRoboRally.GameTerminal.Config
{
    interface IConfig
    {
        ushort Port { get; }

        bool UseHttps { get; }

        bool IncludeUpgrades { get; set; }

        int MinProgrammingCardCount { get; }

        List<KeyValuePair<string, int>> ProgrammingPackDefaults { get; }

        List<KeyValuePair<string, int>> ProgrammingPackLastUsed { get; set; }
    }
}
