﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for AllPushPanels.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class AllPushPanels : UserControl
    {
        public AllPushPanels()
        {
            InitializeComponent();
        }
    }
}
