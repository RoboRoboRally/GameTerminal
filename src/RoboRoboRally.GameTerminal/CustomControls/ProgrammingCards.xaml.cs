﻿using RoboRoboRally.GameTerminal.Models;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for RobotCards.xaml
    /// </summary>
    /// 
    [CompilerGenerated]
    public partial class ProgrammingCards : UserControl
    {
        public ObservableCollection<SlotModel> SelectedCards
        {
            get { return (ObservableCollection<SlotModel>)GetValue(selectedCardsProperty); }
            set { SetValue(selectedCardsProperty, value); }
        }

        public static readonly DependencyProperty selectedCardsProperty =
            DependencyProperty.Register(nameof(SelectedCards), typeof(ObservableCollection<SlotModel>), typeof(ProgrammingCards), new PropertyMetadata(null));

        public ProgrammingCards()
        {
            InitializeComponent();
            CardGrid.DataContext = this;
        }
    }
}
