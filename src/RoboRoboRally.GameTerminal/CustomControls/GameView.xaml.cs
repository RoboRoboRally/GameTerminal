﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for GameView.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class GameView : UserControl
    {
        public GameView()
        {
            InitializeComponent();
        }
    }
}
