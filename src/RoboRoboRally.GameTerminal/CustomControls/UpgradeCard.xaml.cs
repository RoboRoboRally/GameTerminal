﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for UpgradeCard.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class UpgradeCard : UserControl
    {
        public UpgradeCard()
        {
            InitializeComponent();
        }
    }
}
