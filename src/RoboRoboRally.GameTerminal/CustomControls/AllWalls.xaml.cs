﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for AllWalls.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class AllWalls : UserControl
    {
        public AllWalls()
        {
            InitializeComponent();
        }
    }
}
