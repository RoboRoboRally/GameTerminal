﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for Card.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class Card : UserControl
    {
        public Card()
        {
            InitializeComponent();
        }
    }
}
