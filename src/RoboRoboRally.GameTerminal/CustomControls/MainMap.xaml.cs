﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for MainMap.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class MainMap : UserControl
    {
        public MainMap()
        {
            InitializeComponent();
        }
    }
}
