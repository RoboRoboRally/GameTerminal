﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for AllLasers.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class AllLasers : UserControl
    {
        public AllLasers()
        {
            InitializeComponent();
        }
    }
}
