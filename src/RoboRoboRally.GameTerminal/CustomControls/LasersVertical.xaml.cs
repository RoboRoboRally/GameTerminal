﻿using RoboRoboRally.GameTerminal.Models;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for Lasers.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class LasersVertical : UserControl
    {
        public int PositionV
        {
            get { return (int)GetValue(position); }
            set { SetValue(position, value); }
        }

        public static readonly DependencyProperty position =
            DependencyProperty.Register(nameof(PositionV), typeof(int), typeof(LasersVertical), new PropertyMetadata(null));

        public TileModel TileV
        {
            get { return (TileModel)GetValue(tile); }
            set { SetValue(tile, value); }
        }

        public static readonly DependencyProperty tile =
            DependencyProperty.Register(nameof(TileV), typeof(TileModel), typeof(LasersVertical), new PropertyMetadata(null));


        public LasersVertical()
        {
            InitializeComponent();
            LaserGrid.DataContext = this;
        }
    }
}
