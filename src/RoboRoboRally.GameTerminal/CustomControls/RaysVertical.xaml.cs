﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for RaysVertical.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class RaysVertical : UserControl
    {
        public RaysVertical()
        {
            InitializeComponent();
        }
    }
}
