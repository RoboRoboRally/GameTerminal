﻿using RoboRoboRally.GameTerminal.Models;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for Upgrades.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class Upgrades : UserControl
    {
        public ObservableCollection<SlotModel> UpgradeCards
        {
            get { return (ObservableCollection<SlotModel>)GetValue(upgradeCardsProperty); }
            set { SetValue(upgradeCardsProperty, value); }
        }

        public static readonly DependencyProperty upgradeCardsProperty =
            DependencyProperty.Register(nameof(UpgradeCards), typeof(ObservableCollection<SlotModel>), typeof(Upgrades), new PropertyMetadata(null));

        public Upgrades()
        {
            InitializeComponent();
            UpgradeGrid.DataContext = this;
        }
    }
}
