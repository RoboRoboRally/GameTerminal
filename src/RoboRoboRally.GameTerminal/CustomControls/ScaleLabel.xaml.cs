﻿using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for ScaleLabel.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class ScaleLabel : UserControl
    {
        public string Text
        {
            get { return (string)GetValue(text); }
            set { SetValue(text, value); }
        }

        public static readonly DependencyProperty text =
            DependencyProperty.Register(nameof(Text), typeof(string), typeof(ScaleLabel), new PropertyMetadata(null));

        public int Size
        {
            get { return (int)GetValue(size); }
            set { SetValue(size, value); }
        }

        public static readonly DependencyProperty size =
            DependencyProperty.Register(nameof(Size), typeof(int), typeof(ScaleLabel), new PropertyMetadata(null));

        public ScaleLabel()
        {
            InitializeComponent();
            LabelGrid.DataContext = this;
        }
    }
}
