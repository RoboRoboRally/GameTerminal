﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for RaysHorizontal.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class RaysHorizontal : UserControl
    {
        public RaysHorizontal()
        {
            InitializeComponent();
        }
    }
}
