﻿using RoboRoboRally.GameTerminal.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for Plugins.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class Plugins : UserControl
    {
        GameWindowViewModel ViewModel => DataContext as GameWindowViewModel;

        public Plugins()
        {
            InitializeComponent();
            DataContextChanged += GameWindow_DataContextChanged;

        }

        private void GameWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var plugins = ViewModel.Lobby.GetAvailablePlugins();
            AddPluginInfos(AvailablePlugins, plugins);
        }

        private void AddPluginInfos(ListView view, IEnumerable<object> plugins)
        {
            view.Items.Clear();
            foreach (var plugin in plugins)
                view.Items.Add(plugin);
        }

        private void HidePanel(object sender, RoutedEventArgs args)
        {
            ViewModel.PluginsWindowVisibility = Visibility.Collapsed;
        }

        private async void AddPlugin(object sender, RoutedEventArgs args)
        {
            ViewModel.PluginBeingManipulated = true;
            var itemButton = sender as FrameworkElement;
            var info = itemButton.DataContext as PluginInfo;

            var vm = ViewModel;
            var plugins = await Task.Run(() =>
            vm.Lobby.AddPlugin(info));

            AvailablePlugins.Items.Clear();
            AddPluginInfos(AvailablePlugins, plugins);

            RefreshPluginViews();
            ViewModel.PluginBeingManipulated = false;
        }

        private async void RemovePlugin(object sender, RoutedEventArgs args)
        {
            ViewModel.PluginBeingManipulated = true;
            var item = sender as FrameworkElement;
            var info = item.DataContext as InstantiatedPluginInfo;

            var vm = ViewModel;
            await Task.Run(() => vm.Lobby.RemovePlugin(info));

            RefreshPluginViews();
            ViewModel.PluginBeingManipulated = false;
        }

        public void RefreshPluginViews()
        {
            var instantiatedPlugins = ViewModel.Lobby.GetAddedPlugins();
            var availablePlugins = ViewModel.Lobby.GetAvailablePlugins();

            AddPluginInfos(AddedPlugins, instantiatedPlugins);
            AddPluginInfos(AvailablePlugins, availablePlugins);
        }
    }
}
