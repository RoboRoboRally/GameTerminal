﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for StartingMap.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class StartingMap : UserControl
    {
        public StartingMap()
        {
            InitializeComponent();
        }
    }
}
