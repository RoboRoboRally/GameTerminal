﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for TileRow.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class TileRow : UserControl
    {
        public TileRow()
        {
            InitializeComponent();
        }
    }
}
