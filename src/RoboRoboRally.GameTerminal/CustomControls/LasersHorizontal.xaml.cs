﻿using RoboRoboRally.GameTerminal.Models;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.CustomControls
{
    /// <summary>
    /// Interaction logic for Lasers.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class LasersHorizontal : UserControl
    {
        public int PositionH
        {
            get { return (int)GetValue(position); }
            set { SetValue(position, value); }
        }

        public static readonly DependencyProperty position =
            DependencyProperty.Register(nameof(PositionH), typeof(int), typeof(LasersHorizontal), new PropertyMetadata(null));

        public TileModel TileH
        {
            get { return (TileModel)GetValue(tile); }
            set { SetValue(tile, value); }
        }

        public static readonly DependencyProperty tile =
            DependencyProperty.Register(nameof(TileH), typeof(TileModel), typeof(LasersHorizontal), new PropertyMetadata(null));


        public LasersHorizontal()
        {
            InitializeComponent();
            LaserGrid.DataContext = this;
        }
    }
}
