﻿using RoboRoboRally.GameTerminal.ViewModels;
using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RoboRoboRally.GameTerminal.Views
{
    /// <summary>
    /// Interaction logic for GameSetupView.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class GameSetupView : UserControl
    {
        GameSetupViewModel ViewModel => DataContext as GameSetupViewModel;

        public GameSetupView()
        {
            InitializeComponent();
            DataContextChanged += GameSetup_DataContextChanged;
        }

        private void GameSetup_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.Map.PropertyChanged += (s, args) =>
                {
                    if (args.PropertyName == nameof(ViewModel.Map.MainMapGrid) || args.PropertyName == nameof(ViewModel.Map.StartingMapGrid))
                        ViewModel.GameWindowViewModel.RefreshView();
                };
            }
        }

        private void CheckNumericInput(object sender, TextCompositionEventArgs args)
        {
            args.Handled = !new Regex("^[0-9]*$").IsMatch(args.Text);
        }

        private void SetSeed(object sender, TextChangedEventArgs args)
        {
            string text = SeedTextBox.Text;
            if (text.Trim() == string.Empty)
                ViewModel.GameWindowViewModel.Settings.Seed = null;
            else
            {
                if (long.TryParse(text, out long seed))
                    ViewModel.GameWindowViewModel.Settings.Seed = seed;
            }
        }
    }
}
