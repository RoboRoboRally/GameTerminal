﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.Views
{
    /// <summary>
    /// Interaction logic for TileView.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class MapsView : UserControl
    {
        public MapsView()
        {
            InitializeComponent();
        }
    }
}
