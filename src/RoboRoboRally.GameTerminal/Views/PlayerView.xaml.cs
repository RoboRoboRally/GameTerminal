﻿using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace RoboRoboRally.GameTerminal.Views
{
    /// <summary>
    /// Interaction logic for PlayerView.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class PlayerView : UserControl
    {
        public PlayerView()
        {
            InitializeComponent();
        }
    }
}
