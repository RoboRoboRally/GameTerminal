﻿using RoboRoboRally.GameTerminal.ViewModels;
using System.Runtime.CompilerServices;
using System.Windows;

namespace RoboRoboRally.GameTerminal
{
    /// <summary>
    /// Interaction logic for GameWindowView.xaml
    /// </summary>
    [CompilerGenerated]
    public partial class GameWindowView : Window
    {
        GameWindowViewModel ViewModel => DataContext as GameWindowViewModel;
        bool isInitialized = false;

        public GameWindowView()
        {
            DataContextChanged += GameWindow_DataContextChanged;
        }

        private void GameWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!isInitialized)
            {
                InitializeComponent();
                isInitialized = true;
                ViewModel.Log = Log;
                ViewModel.PluginsWindow = PluginsWindow;
            }

            ViewModel.GameWindow_DataContextChanged();
        }
    }
}
