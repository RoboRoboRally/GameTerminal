﻿namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// Data needed for a player to connect to the game.
    /// Mobile app reads this data through QR code scan.
    /// </summary>
    public class PlayerQrConnectionData
    {
        /// <summary>
        /// Ipv4 of a server
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// Port where the server listens
        /// </summary>
        public ushort Port { get; set; }
        /// <summary>
        /// Name of the lobby, where players will join
        /// </summary>
        public string LobbyName { get; set; }
        /// <summary>
        /// Name of a robot, which player can use
        /// </summary>
        public string RobotName { get; set; }
    }
}
