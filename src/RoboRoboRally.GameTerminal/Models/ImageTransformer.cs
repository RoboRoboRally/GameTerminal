﻿using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// This class handles image manipulations
    /// </summary>
    public static class ImageTransformer
    {        
        /// <summary>
        /// Cenversion of bitmaps into ImageSource is needed in converters. We can load textures as bitmaps
        /// from the hard drive, but we need ImageSource in order to draw them.
        /// This code was coppied from https://stackoverflow.com/questions/37890121/fast-conversion-of-bitmap-to-imagesource
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>
        public static BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
                return bitmapimage;
            }
        }

        /// <summary>
        /// RotationFlipType is needed for rotation of textures in converters
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static RotateFlipType GetRotationFlipTypeFromDirectionType(DirectionType value)
        {
            switch (value)
            {
                case DirectionType.Up:
                    return RotateFlipType.RotateNoneFlipNone;
                case DirectionType.Down:
                    return RotateFlipType.Rotate180FlipNone;
                case DirectionType.Right:
                    return RotateFlipType.Rotate90FlipNone;
                case DirectionType.Left:
                    return RotateFlipType.Rotate270FlipNone;
                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }
    }
}
