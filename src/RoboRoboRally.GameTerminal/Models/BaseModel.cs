﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// Class which is used for inheritance
    /// Implement the basic functionality for PropertyChanged
    /// </summary>
    public class BaseModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The event, which is invoked in case we set a new value to a property
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Invokes the PropertyChanged for a given property
        /// </summary>
        /// <param name="propertyName">name used in PropertyChanged</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Generic setter of a property
        /// Compares old value with new one
        /// If new value is different invokes PropertyChanged
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="backingFiled">old value</param>
        /// <param name="value">new value</param>
        /// <param name="propertyName">name used in PropertyChanged</param>
        protected void SetValue<T>(ref T backingField, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, value))
                return;
            backingField = value;
            OnPropertyChanged(propertyName);
        }
    }
}