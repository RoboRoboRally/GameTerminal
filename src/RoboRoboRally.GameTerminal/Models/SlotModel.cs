﻿using System.Windows.Media;

namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// This class represents the card on the screen.
    /// </summary>
    public class SlotModel : BaseModel
    {
        private CardModel card;
        public CardModel Card
        {
            get { return card; }
            set { SetValue(ref card, value, nameof(Card)); }
        }

        private string cardColor;
        /// <summary>
        /// Color of the card - the same as the robot color
        /// </summary>
        public string CardColor
        {
            get { return cardColor; }
            set { SetValue(ref cardColor, value, nameof(CardColor)); }
        }

        public SlotModel(string cardColor, CardModel card)
        {
            this.CardColor = cardColor;
            this.Card = card;
        }
    }
}
