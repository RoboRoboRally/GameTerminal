﻿namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// Class used to count the cards in a pack during game setup
    /// </summary>
    public class ProgrammingCardCount : BaseModel
    {
        private string cardName;
        /// <summary>
        /// Name of the card, which is counted
        /// Used in the game setup
        /// </summary>
        public string CardName
        {
            get => cardName;
            set => SetValue(ref cardName, value);
        }

        private int cardCount;
        /// <summary>
        /// Number of certain cards in the pack
        /// Used to indicate (together with counts of other cards), if the game can start (game cannot start with less than 10 cards)
        /// /// Used in the game setup
        /// </summary>
        public int CardCount
        {
            get => cardCount;
            set => SetValue(ref cardCount, value);
        }
    }
}
