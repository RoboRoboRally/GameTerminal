﻿namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// Constants which we used in the appliaction
    /// </summary>
    static class Constants
    {
        public static string programmingCardBack = "ProgrammingCardBack";
        public static string upgradeCardBack = "UpgradeCardBack";
        public static string invalidTexture = "InvalidTexture";
        public static string railGun = "RailGun";
        public static string rearLaser = "RearLaser";
        public static string backgroundColor= "Transparent";
    }
}
