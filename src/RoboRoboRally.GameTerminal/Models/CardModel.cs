﻿
namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// Class used to hold CardId of a card on the screen
    /// </summary>
    public class CardModel : BaseModel
    {
        private string cardId;
        public string CardId
        {
            get { return cardId; }
            set { SetValue(ref cardId, value, nameof(CardId)); }
        }

        public CardModel(string CardId)
        {
            this.CardId = CardId;
        }
    }
}
