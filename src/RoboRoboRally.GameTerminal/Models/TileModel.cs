﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;

namespace RoboRoboRally.GameTerminal.Models
{
    /// <summary>
    /// Information about a tile needed in order to draw it on the game board
    /// </summary>
    public class TileModel : BaseModel
    {
        /// <summary>
        /// Which map this tile belongs to
        /// </summary>
        private IMap map = null;
        public IMap Map
        {
            get { return map; }
            set { SetValue(ref map, value, nameof(Map)); }
        }

        private int x = 0;
        public int X
        {
            get { return x; }
            set { SetValue(ref x, value, nameof(X)); }
        }

        private int y = 0;
        public int Y
        {
            get { return y; }
            set { SetValue(ref y, value, nameof(Y)); }
        }

        private int horizontalRays = 0;
        /// <summary>
        /// Number of laser rays going through the tile in horizontal way
        /// </summary>
        public int HorizontalRays
        {
            get { return horizontalRays; }
            set { SetValue(ref horizontalRays, value, nameof(HorizontalRays)); }
        }

        private int verticalRays = 0;
        /// <summary>
        /// Number of laser rays going through the tile in vertical way
        /// </summary>
        public int VerticalRays
        {
            get { return verticalRays; }
            set { SetValue(ref verticalRays, value, nameof(VerticalRays)); }
        }

        /// <summary>
        /// Retursn Tile from the IMap Map with the coordinates of this TileModel
        /// </summary>
        /// <returns></returns>
        public Tile GetModelTile()
        {
            return Map[Y, X];
        }

        public TileModel(IMap map, int x, int y)
        {
            Map = map;
            X = x;
            Y = y;
        }
    }
}
